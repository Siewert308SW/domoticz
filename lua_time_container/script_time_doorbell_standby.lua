--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_doorbell_standby.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 28-12-2016
	@Time script to virtual set doorbell standby OFF so frontdoor light lumen can be decreased
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Doorbell Standby Settings
-- **********************************************************
--	

local doorbell_standby				= 'Deurbel - Standby'
local doorbell_standby_counter 		= 'IsDoorbell_Standby_Counter'
local frontdoor						= 'Voor Deur'
local doorbell_standby_timeout		= 2 --(1 minute counter)

--
-- **********************************************************
-- Doorbell Standby Counter
-- **********************************************************
--

doorbell_standby_minutes = tonumber(uservariables[doorbell_standby_counter])
doorbell_standby_remaining = doorbell_standby_timeout - doorbell_standby_minutes

if (otherdevices[doorbell_standby] == 'On' and otherdevices[frontdoor] == 'Closed') then
	doorbell_standby_minutes = doorbell_standby_minutes + 1

if verbose == 1 then	
	print('<font color="Darkgreen">-- '..doorbell_standby..' ==> '..doorbell_standby..' ON, In aprox ' ..tonumber(doorbell_standby_remaining).. ' minutes '..doorbell_standby..' will be switched OFF!</font>')	
end
	commandArray['Variable:' .. doorbell_standby_counter] = tostring(doorbell_standby_minutes)
end 

----------

if otherdevices[doorbell_standby] == 'On' and doorbell_standby_minutes > tonumber(doorbell_standby_timeout) then
 	commandArray[doorbell_standby]='Off'

if verbose == 1 then	
	print('<font color="Darkgreen"><font color="Darkgreen">-- '..doorbell_standby..' ==> '..doorbell_standby..' was ON for ' ..tonumber(doorbell_standby_minutes).. ' minutes, '..doorbell_standby..' switched OFF</font>')	
end
	doorbell_standby_minutes = 0
	commandArray['Variable:' .. doorbell_standby_counter] = tostring(doorbell_standby_minutes)	
end

----------

if otherdevices[doorbell_standby] == 'Off' and doorbell_standby_minutes > 0 then

if verbose == 1 then
	print('<font color="Darkgreen"><font color="Darkgreen">-- '..doorbell_standby..' ==> Counter has been reset!</font>')	
end	
	doorbell_standby_minutes = 0
	commandArray['Variable:' .. doorbell_standby_counter] = tostring(doorbell_standby_minutes)	
end
	