--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_shower_light.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-10-2017
	@Time script to switch OFF shower light if forgotten to turn OFF manually or missed KaKu/CoCo signal
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Toilet Settings
-- **********************************************************
--	

local shower_nomotion_counter 		= 'IsDouche_LightCounter'
local shower_light					= 'Douche Lamp'
local shower_nomotion_timeout		= 70 --(1 minute counter)

--
-- **********************************************************
-- Toilet No Motion Counter
-- **********************************************************
--

shower_motion_minutes = tonumber(uservariables[shower_nomotion_counter])
shower_remaining = shower_nomotion_timeout - shower_motion_minutes

if (otherdevices[shower_light] == 'On') then
	shower_motion_minutes = shower_motion_minutes + 1

		if verbose == 1 then	
			print('<font color="Brown">-- '..shower_light..' ==> '..shower_light..' ON, It will be switched OFF in aprox ' ..tonumber(shower_remaining).. ' minutes...</font>')	
		end
	commandArray['Variable:' .. shower_nomotion_counter] = tostring(shower_motion_minutes)
end 

----------

if otherdevices[shower_light] == 'On' and shower_motion_minutes > tonumber(shower_nomotion_timeout) then
 	commandArray[shower_light]='Off'
	
		if verbose == 1 then	
			print('<font color="Brown"><font color="Brown">-- '..shower_light..' ==> '..shower_light..' ON for ' ..tonumber(shower_motion_minutes).. ' minutes, '..shower_light..' switched OFF</font>')	
		end
	shower_motion_minutes = 0
	commandArray['Variable:' .. shower_nomotion_counter] = tostring(shower_motion_minutes)	
end

----------

if otherdevices[shower_light] == 'Off' and shower_motion_minutes > 0 and shower_motion_minutes < tonumber(shower_nomotion_timeout) then
	
		if verbose == 1 then	
			print('<font color="Brown"><font color="Brown">-- '..shower_light..' ==> '..shower_light..' switched OFF manually before reaching max runtime, Counter has been reset!</font>')	
		end
	shower_motion_minutes = 0
	commandArray['Variable:' .. shower_nomotion_counter] = tostring(shower_motion_minutes)	
end
	