--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_nobody_home.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch on dummy NobodyHome to virtual close your house and switch ON/OFF standbykillers
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

--
-- **********************************************************
-- Nobody Home Settings
-- **********************************************************
--

-- Logging: 0 = None, 1 = All
local verbose = 1	

local nobody_home					= 'Niemand Thuis'
local nobody_home_nomotion_counter 	= 'IsNobody_Home_NoMotionCounter'
local someonehome					= 'Iemand Thuis'
local someonehome_standby			= 'Iemand Thuis - Standby'
local nobody_home_nomotion_timeout	= 10 --(1 minute counter)

--
-- **********************************************************
-- Nobody Home No Motion Counter
-- **********************************************************
--

nobody_home_motion_minutes = tonumber(uservariables[nobody_home_nomotion_counter])
nobody_home_remaining = nobody_home_nomotion_timeout - nobody_home_motion_minutes

if (otherdevices[nobody_home] == 'Off' and otherdevices[someonehome] == 'Off' and otherdevices[someonehome_standby] == 'Off') then
	nobody_home_motion_minutes = nobody_home_motion_minutes + 1

if verbose == 1 then	
	print('<font color="blue">-- '..nobody_home..' ==> No one home for ' ..tonumber(nobody_home_motion_minutes).. ' minutes, In aprox ' ..tonumber(nobody_home_remaining).. ' minutes your home will be virtualy shut down!</font>')	
end

	commandArray['Variable:' .. nobody_home_nomotion_counter] = tostring(nobody_home_motion_minutes)
end 

----------

if otherdevices[nobody_home] == 'Off' and otherdevices[someonehome] == 'Off' and otherdevices[someonehome_standby] == 'Off' and nobody_home_motion_minutes > tonumber(nobody_home_nomotion_timeout) then
 	commandArray[nobody_home]='On'

if verbose == 1 then
	print('<font color="blue"><font color="blue">-- '..nobody_home..' ==> No one home for ' ..tonumber(nobody_home_motion_minutes).. ' minutes, Your home has been virtually shut down!</font>')	
end

	nobody_home_motion_minutes = 0
	commandArray['Variable:' .. nobody_home_nomotion_counter] = tostring(nobody_home_motion_minutes)	
end

----------

if otherdevices[nobody_home] == 'On' and otherdevices[someonehome] == 'Off' and otherdevices[someonehome_standby] == 'Off' and nobody_home_motion_minutes > 0 then

if verbose == 1 then
	print('<font color="blue"><font color="blue">-- '..nobody_home..' ==> Counter has been reset!</font>')	
end

	nobody_home_motion_minutes = 0
	commandArray['Variable:' .. nobody_home_nomotion_counter] = tostring(nobody_home_motion_minutes)	
end

----------

if otherdevices[nobody_home] == 'On' and otherdevices[someonehome] == 'On' and otherdevices[someonehome_standby] == 'Off' then

if verbose == 1 then
	print('<font color="blue"><font color="blue">-- '..nobody_home..' ==> Some one home, Counter has been reset!</font>')
end	
	
	commandArray[nobody_home]='Off'	
	nobody_home_motion_minutes = 0
	commandArray['Variable:' .. nobody_home_nomotion_counter] = tostring(nobody_home_motion_minutes)	
end

----------

if otherdevices[nobody_home] == 'Off' and otherdevices[someonehome] == 'On' and otherdevices[someonehome_standby] == 'Off' and nobody_home_motion_minutes > 0 then

if verbose == 1 then
	print('<font color="blue"><font color="blue">-- '..nobody_home..' ==> Some one home, Counter has been reset!</font>')
 end	
	
	commandArray[nobody_home]='Off'	
	nobody_home_motion_minutes = 0
	commandArray['Variable:' .. nobody_home_nomotion_counter] = tostring(nobody_home_motion_minutes)	
end
	