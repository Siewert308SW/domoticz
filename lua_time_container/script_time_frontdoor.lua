--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_frontdoor.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to virtual close KaKu/CoCo door sensor if signal missed so a new event can be triggered anyways
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Frontdoor Settings
-- **********************************************************
--	

local frontdoor						= 'Voor Deur'
local frontdoor_nomotion_counter 	= 'IsFrontdoor_NoMotionCounter'
local livingroom_door				= 'Kamer Deur'
local frontdoor_nomotion_timeout	= 15 --(1 minute counter)

--
-- **********************************************************
-- Frontdoor No Motion Counter
-- **********************************************************
--

frontdoor_motion_minutes = tonumber(uservariables[frontdoor_nomotion_counter])
frontdoor_remaining = frontdoor_nomotion_timeout - frontdoor_motion_minutes

if (otherdevices[frontdoor] == 'Open' and otherdevices[livingroom_door] == 'Closed') then
	frontdoor_motion_minutes = frontdoor_motion_minutes + 1
		if verbose == 1 then	
		print('<font color="blue">-- '..frontdoor..' ==> '..frontdoor..' open, It will be virtually closed in about ' ..tonumber(frontdoor_remaining).. ' minutes...</font>')	
		end
	commandArray['Variable:' .. frontdoor_nomotion_counter] = tostring(frontdoor_motion_minutes)
end 

----------

if otherdevices[frontdoor] == 'Open' and otherdevices[livingroom_door] == 'Closed' and frontdoor_motion_minutes > tonumber(frontdoor_nomotion_timeout) then
 	commandArray[frontdoor]='Off'
		if verbose == 1 then	
		print('<font color="blue">-- '..frontdoor..' ==> '..frontdoor..' still open, It will be virtually closed now...</font>')	
		end
	frontdoor_motion_minutes = 0
	commandArray['Variable:' .. frontdoor_nomotion_counter] = tostring(frontdoor_motion_minutes)	
end

----------

if otherdevices[frontdoor] == 'Closed' and frontdoor_motion_minutes > 0 then
		if verbose == 1 then	
		print('<font color="blue">-- '..frontdoor..' ==> '..frontdoor..' closed, Counter has been reset...</font>')	
		end
	frontdoor_motion_minutes = 0
	commandArray['Variable:' .. frontdoor_nomotion_counter] = tostring(frontdoor_motion_minutes)	
end

	