--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_arrival.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch OFF dummy which is triggered when someone came home but no devices online
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

--
-- **********************************************************
-- Arrival Home Settings
-- **********************************************************
--	

-- Logging: 0 = None, 1 = All
local verbose = 1

local arrival_standby					= 'Aankomst - Standby'
local arrival_standby_nomotion_counter 	= 'IsArrival_Home_NoMotionCounter'
local someonehome						= 'Iemand Thuis'
local someonehome_standby				= 'Iemand Thuis - Standby'

-- Devices
local laptop_switch 					= 'Laptops'
local television 						= 'Televisie'
local phone_switch 						= 'Telefoons'

-- Timeout
local arrival_standby_nomotion_timeout	= 30 --(1 minute counter)

--
-- **********************************************************
-- Arrival Home No Motion Counter
-- **********************************************************
--

arrival_standby_motion_minutes = tonumber(uservariables[arrival_standby_nomotion_counter])
arrival_standby_remaining = arrival_standby_nomotion_timeout - arrival_standby_motion_minutes

if  otherdevices[arrival_standby] == 'On' 
	and otherdevices[someonehome] == 'On' 
	and otherdevices[someonehome_standby] == 'Off'
	and otherdevices[phone_switch] == 'Off' then
	arrival_standby_motion_minutes = arrival_standby_motion_minutes + 1

	if verbose == 1 then	
	print('<font color="blue">-- '..arrival_standby..' ==> Some home home for ' ..tonumber(arrival_standby_motion_minutes).. ' minutes but no devices ONLINE, '..someonehome..' will be switched OFF in aprox ' ..tonumber(arrival_standby_remaining).. ' minutes...</font>')	
	end
	commandArray['Variable:' .. arrival_standby_nomotion_counter] = tostring(arrival_standby_motion_minutes)
end 

----------

if otherdevices[arrival_standby] == 'On' 
	and otherdevices[someonehome] == 'On' 
	and otherdevices[someonehome_standby] == 'Off'
	and otherdevices[phone_switch] == 'Off' 
	and arrival_standby_motion_minutes > tonumber(arrival_standby_nomotion_timeout) then
 	commandArray[someonehome]='Off AFTER 5'	
 	commandArray[arrival_standby]='Off'

if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..arrival_standby..' ==> Some one home for ' ..tonumber(arrival_standby_motion_minutes).. ' minutes but still no devices ONLINE, '..someonehome..' switched OFF...</font>')	
end	
	
	arrival_standby_motion_minutes = 0
	commandArray['Variable:' .. arrival_standby_nomotion_counter] = tostring(arrival_standby_motion_minutes)	
end

----------

if otherdevices[arrival_standby] == 'On' 
	and otherdevices[someonehome] == 'On' 
	and otherdevices[someonehome_standby] == 'Off'
	and otherdevices[phone_switch] == 'On' 
	and arrival_standby_motion_minutes > 0 then
 	commandArray[arrival_standby]='Off'

if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..arrival_standby..' ==> A phone is online, Counter has been reset!</font>')
end
 	commandArray[arrival_standby]='Off'		
	arrival_standby_motion_minutes = 0
	commandArray['Variable:' .. arrival_standby_nomotion_counter] = tostring(arrival_standby_motion_minutes)	
	
elseif otherdevices[arrival_standby] == 'On' 
	and otherdevices[someonehome] == 'On' 
	and otherdevices[someonehome_standby] == 'Off'
	and otherdevices[phone_switch] == 'On' 
	and arrival_standby_motion_minutes == 0 then

if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..arrival_standby..' ==> A phone is online, Counter has been reset!</font>')
end
 	commandArray[arrival_standby]='Off'			
	arrival_standby_motion_minutes = 0
	commandArray['Variable:' .. arrival_standby_nomotion_counter] = tostring(arrival_standby_motion_minutes)		
end
	