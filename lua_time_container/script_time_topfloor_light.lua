--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_topfloor_light.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch OFF '..topfloor_light..' after x minutes
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- topfloor Settings
-- **********************************************************
--	

local topfloor_nomotion_counter 		= 'Istopfloor_LightCounter'
local topfloor_light					= 'Gang Lamp Boven'
local topfloor_light_standby			= 'Gang Lamp Boven - Standby'
local topfloor_nomotion_timeout			= 5 --(1 minute counter)

--
-- **********************************************************
-- topfloor No Motion Counter
-- **********************************************************
--

topfloor_motion_minutes = tonumber(uservariables[topfloor_nomotion_counter])
topfloor_remaining = topfloor_nomotion_timeout - topfloor_motion_minutes

if (otherdevices[topfloor_light_standby] == 'On') then
	topfloor_motion_minutes = topfloor_motion_minutes + 1

if verbose == 1 then	
	print('<font color="blue">-- '..topfloor_light..' ==> '..topfloor_light..' ON for ' ..tonumber(topfloor_motion_minutes).. ' minutes, In aprox ' ..tonumber(topfloor_remaining).. ' minutes '..topfloor_light..' will be switched OFF!</font>')	
end
	
	commandArray['Variable:' .. topfloor_nomotion_counter] = tostring(topfloor_motion_minutes)
end 

----------

if otherdevices[topfloor_light_standby] == 'On'
	and topfloor_motion_minutes > tonumber(topfloor_nomotion_timeout) then
		commandArray[topfloor_light_standby]='Off'
		commandArray[topfloor_light]='Off AFTER 5'	

if verbose == 1 then		
	print('<font color="blue"><font color="blue">-- '..topfloor_light..' ==> '..topfloor_light..' max runtime exceeded, '..topfloor_light..' switched OFF</font>')	
end
	
	topfloor_motion_minutes = 0
	commandArray['Variable:' .. topfloor_nomotion_counter] = tostring(topfloor_motion_minutes)	
end

----------

if otherdevices[topfloor_light_standby] == 'Off' and topfloor_motion_minutes > 0 then
	
if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..topfloor_light..' ==> '..topfloor_light..' switched OFF manually before reaching max runtime, Counter has been reset!</font>')	
end
	
	topfloor_motion_minutes = 0
	commandArray['Variable:' .. topfloor_nomotion_counter] = tostring(topfloor_motion_minutes)	
end
	