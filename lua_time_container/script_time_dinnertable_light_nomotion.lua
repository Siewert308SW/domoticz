--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_dinnertable_light_nomotion.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 01-19-2016
	@Time script to switch OFF dinnertable light when no motion detected at the dinnertable
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Dinnertable Light Standby Settings
-- **********************************************************
--	

local dinner_table_light 						= 'Woonkamer Eettafel Lamp'
local dinnertable_light_standby					= 'IsDinnerTable_Light_Standby'
local dinnertable_light_standby_counter 		= 'IsDinnerTable_Light_Standby_Counter'
local laptop_switch								= 'Laptops'
local dinnertable_light_standby_timeout			= 10 --(1 minute counter)

--
-- **********************************************************
-- Dinnertable Light Standby Counter
-- **********************************************************
--

dinnertable_light_standby_minutes = tonumber(uservariables[dinnertable_light_standby_counter])
dinnertable_light_standby_remaining = dinnertable_light_standby_timeout - dinnertable_light_standby_minutes

if (otherdevices[laptop_switch] == 'Off' and uservariables[dinnertable_light_standby] == 1 and otherdevices[dinner_table_light] ~= 'Off') then
	dinnertable_light_standby_minutes = dinnertable_light_standby_minutes + 1

if verbose == 1 then	
	print('<font color="Darkgreen">-- '..dinner_table_light..' ==> '..dinner_table_light..' ON, But no motion detected: In aprox ' ..tonumber(dinnertable_light_standby_remaining).. ' minutes '..dinner_table_light..' will be switched OFF!</font>')	
end
	commandArray['Variable:' .. dinnertable_light_standby_counter] = tostring(dinnertable_light_standby_minutes)
end 

----------

if otherdevices[laptop_switch] == 'Off' and uservariables[dinnertable_light_standby] == 1 and otherdevices[dinner_table_light] ~= 'Off' and dinnertable_light_standby_minutes > tonumber(dinnertable_light_standby_timeout) then
 	commandArray[dinner_table_light]='Off'
	commandArray['Variable:' .. dinnertable_light_standby] = '0'
if verbose == 1 then	
	print('<font color="Darkgreen"><font color="Darkgreen">-- '..dinner_table_light..' ==> Still no motion detected, '..dinner_table_light..' switched OFF</font>')	
end
	dinnertable_light_standby_minutes = 0
	commandArray['Variable:' .. dinnertable_light_standby_counter] = tostring(dinnertable_light_standby_minutes)	
end

----------

if otherdevices[laptop_switch] == 'Off' and uservariables[dinnertable_light_standby] == 1 and otherdevices[dinner_table_light] == 'Off' and dinnertable_light_standby_minutes > 0 then

if verbose == 1 then
	print('<font color="Darkgreen"><font color="Darkgreen">-- '..dinner_table_light..' ==> '..dinner_table_light..' switched OFF, Counter has been reset!</font>')	
end	
	dinnertable_light_standby_minutes = 0
	commandArray['Variable:' .. dinnertable_light_standby_counter] = tostring(dinnertable_light_standby_minutes)	
	commandArray['Variable:' .. dinnertable_light_standby] = '0'	
end

if otherdevices[laptop_switch] == 'On' and uservariables[dinnertable_light_standby] == 1 and otherdevices[dinner_table_light] ~= 'Off' then

if verbose == 1 then
	print('<font color="Darkgreen"><font color="Darkgreen">-- '..dinner_table_light..' ==> '..laptop_switch..' activated, Counter has been reset!</font>')	
end	
	dinnertable_light_standby_minutes = 0
	commandArray['Variable:' .. dinnertable_light_standby_counter] = tostring(dinnertable_light_standby_minutes)	
	commandArray['Variable:' .. dinnertable_light_standby] = '0'
	commandArray[dinner_table_light]='Set Level 7 AFTER 5'	
end
	