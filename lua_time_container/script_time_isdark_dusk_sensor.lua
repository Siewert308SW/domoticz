--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_dusk_sensor.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch ON/OFF dusk sensor if signal is missed taking in count WeatherUnderground Solar indication
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Get System Time
	time = os.date("*t")

-- Logging: 0 = None, 1 = All
local verbose = 1
	
--
-- **********************************************************
-- Nobody Home Settings
-- **********************************************************
--	

local dusk_sensor 					= 'Schemer Sensor'
local dusk_trigger 					= 'Sunrise/Sunset'
local dusk_counter 					= 'IsSchemerSensor_Counter'
local daytime						= 'Var_IsTimeOfTheDay'
local isdark_dinner_table 			= 'IsDonker_Eettafel'
local isdark_living_room_trigger1	= 'IsDonker_Woonkamer_1'
local isdark_living_room_trigger2	= 'IsDonker_Woonkamer_2'
local isdark_front_border_trigger 	= 'IsDonker_Border_Verlichting'
local isdark_back_garden_trigger 	= 'IsDonker_Tuin_Verlichting'
local isdark_xmas_lights 			= 'IsDonker_Kerst_Verlichting'
local dusk_timeout					= 1 --(1 minuten counter) 

local solar 						= 'Solar'
local dusk_solarvalue				= '15'
local dusk_solarvalue_low			= '10'

    sSolar = otherdevices_svalues[solar]:match("([^;]+)")
    sSolar = tonumber(sSolar);

	dusk_minutes = tonumber(uservariables[dusk_counter])
	dusk_remaining = dusk_timeout - dusk_minutes	

    if otherdevices[dusk_sensor] =='Off' and otherdevices[dusk_trigger] == 'On' and sSolar <= tonumber (dusk_solarvalue) then
	dusk_minutes = dusk_minutes + 1

if verbose == 1 then	
	print('<font color="orange">-- '..dusk_sensor..' ==> '..dusk_sensor..' will be activated in about ' ..tonumber(dusk_remaining).. '...</font>')	
end
	commandArray['Variable:' .. dusk_counter] = tostring(dusk_minutes)	
	end

----------
	
	if otherdevices[dusk_sensor] == 'Off' and otherdevices[dusk_trigger] == 'On' and dusk_minutes > tonumber(dusk_timeout) then
 	commandArray[dusk_sensor]='On'	

if verbose == 1 then	
	print('<font color="orange">-- '..dusk_sensor..' ==> '..dusk_sensor..' activated...</font>')		
end
	dusk_minutes = 0
	commandArray['Variable:' .. dusk_counter] = tostring(dusk_minutes)				
	end

----------

    if otherdevices[dusk_sensor] =='On' and otherdevices[dusk_trigger] =='On' and dusk_minutes > 0 then
	dusk_minutes = 0

if verbose == 1 then	
	print('<font color="orange">-- '..dusk_sensor..' ==> Counter reset...</font>')	
end
	commandArray['Variable:' .. dusk_counter] = tostring(dusk_minutes)	
	end	

--------

	if otherdevices[dusk_sensor] =='On' and (time.hour > 5) and (time.hour < 15) and sSolar >= tonumber (dusk_solarvalue_low) then
	commandArray[dusk_sensor]='Off'

if verbose == 1 then	
	print('<font color="orange">-- '..dusk_sensor..' ==> '..dusk_sensor..' has been deactivated as your solar indicates there to much lux...</font>')
end	
	dusk_minutes = 0
	commandArray['Variable:' .. dusk_counter] = tostring(dusk_minutes)	

	end