--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_backdoor.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 26-12-2016
	@Time script to virtual close KaKu/CoCo door sensor if signal missed so a new event can be triggered anyways
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Backdoor Settings
-- **********************************************************
--
	
local backdoor						= 'Achter Deur'
local backdoor_nomotion_counter 	= 'IsBackdoor_NoMotionCounter'
local scullery_door					= 'Bijkeuken Deur'
local backdoor_nomotion_timeout		= 10 --(1 minute counter)

--
-- **********************************************************
-- Backdoor No Motion Counter
-- **********************************************************
--

backdoor_motion_minutes = tonumber(uservariables[backdoor_nomotion_counter])
backdoor_remaining = backdoor_nomotion_timeout - backdoor_motion_minutes

if (otherdevices[backdoor] == 'Open' and otherdevices[scullery_door] == 'Closed') then
	backdoor_motion_minutes = backdoor_motion_minutes + 1
		if verbose == 1 then	
		print('<font color="blue">-- '..backdoor..' ==> '..backdoor..' open, It will be virtually closed in about ' ..tonumber(backdoor_remaining).. ' minutes...</font>')	
		end
	commandArray['Variable:' .. backdoor_nomotion_counter] = tostring(backdoor_motion_minutes)
end 

----------

if otherdevices[backdoor] == 'Open' and otherdevices[scullery_door] == 'Closed' and backdoor_motion_minutes > tonumber(backdoor_nomotion_timeout) then
 	commandArray[backdoor]='Off'
		if verbose == 1 then	
		print('<font color="blue">-- '..backdoor..' ==> '..backdoor..' still open, It will be virtually closed now...</font>')	
		end
	backdoor_motion_minutes = 0
	commandArray['Variable:' .. backdoor_nomotion_counter] = tostring(backdoor_motion_minutes)	
end

----------

if otherdevices[backdoor] == 'Closed' and backdoor_motion_minutes > 0 then

		if verbose == 1 then	
		print('<font color="blue">-- '..backdoor..' ==> '..backdoor..' closed, Counter has been reset...</font>')	
		end
	backdoor_motion_minutes = 0
	commandArray['Variable:' .. backdoor_nomotion_counter] = tostring(backdoor_motion_minutes)	
end
	