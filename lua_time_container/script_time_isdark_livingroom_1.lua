--[[
 -_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
	script_time_lastupdate.lua
	@author: Nicky Bulthuis
	@since: 2015-01-14
	@version: 0.1
 
	A basic LUA script to set a device status after a specific time has been elapsed since the last update.
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-	
--]]

-- **********************************
-- Settings
-- **********************************

-- Logging: 0 = Geen, 1 = Beetje, 2 = Alles
local verbose = 0

-- De naam van de schemer sensor.
local schemersensor = 'IsDonker_Eettafel' 

-- De naam van de isDonker virtuele switch.
local isdonker = 'IsDonker_Woonkamer_1'

-- De naam van de User Variable voor het tellen van de flipstorm, type integer, zet hem op 0.
local flipstorm = 'IsDonker_Woonkamer_1_Counter'

-- Het aantal minuten/keer dat we de schemer sensor op On willen zien voordat we de IsDonker switch op On zetten.		
local flipstorm_on = 5

-- Het aantal minuten/keer dat we de schemer sensor op Off willen zien voordat we de IsDonker switch op Off zetten.		
local flipstorm_off = 1

--
-- Debug logging.
--
function debug(msg)

	if verbose >= 2 then
		print('Schemer Sensor[DEBUG] ==> ' .. msg)
	end

end

--
-- Info logging.
--
function info(msg)

	if verbose >= 1 then
		print('Schemer Sensor[INFO] ==> ' .. msg)
	end

end

--
-- Error logging.
--
function error(msg)

	print('Schemer Sensor[ERROR] ==> ' .. msg)

end

--
-- Validates the configuration
--
function validate() 

	result = true

	if not uservariables[flipstorm] then
		error('User Variable [' .. flipstorm .. '] bestaat niet.')
		result = false
	end
	
	if not otherdevices[isdonker] then
		error('Device [' .. isdonker .. '] bestaat niet.')
		result = false
	end
	
	if not otherdevices[schemersensor] then
		error('Device [' .. schemersensor .. '] bestaat niet.')
		result = false
	end 


	return result

end

--
-- Tests of de IsDonker mag veranderen.
--
function may_update(is_donker)

	counter = tonumber(uservariables[flipstorm])
	current_counter = counter
	
	-- set the counter.
	if is_donker and counter >= 0 then
		counter = math.min(flipstorm_on, counter + 1)
	elseif not is_donker and counter <= 0 then
		counter = math.max(flipstorm_off * -1, counter - 1)
	else
		counter = 0
	end

	if counter ~= current_counter then
		commandArray['Variable:' .. flipstorm]=tostring(counter)
	end

	result = false
		
	if is_donker and counter >= flipstorm_on then
		result = true
	elseif not is_donker and math.abs(counter) >= flipstorm_off then
		result = true
	end

	debug('UserVar=[' .. flipstorm .. '] Counter=[' .. counter .. '] MayUpdate=[' .. tostring(result) .. ']')
	
	return result

end

--
-- Aanpassen status van IsDonker. .
--
function on_off(is_donker)

	update_allowed = may_update(is_donker)

	info('OnOff State[' .. otherdevices[isdonker] .. '] UpdateAllowed[' .. tostring(update_allowed) ..'] IsDark[' .. tostring(is_donker) .. ']')

	if update_allowed then
		if otherdevices[isdonker] == 'Off' and is_donker then
 			commandArray[isdonker]='On'
		elseif (otherdevices[isdonker] == 'On' and not is_donker) then
 			commandArray[isdonker]='Off'
		end
	end

end

--
-- Is de Schemer Sensor On.
--
function is_schemersensor_aan()

	result = false
	result = otherdevices[schemersensor] == 'On'
    debug('SchemerSensor Current[' .. otherdevices[schemersensor] .. '] = ' .. tostring(result))
    return result
end

--commandArray = {}

if validate() then
	on_off(is_schemersensor_aan())
end
--return commandArray