--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_variables.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-4-2017
	@Time script to various user_variables which are used in various lua events
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Get System Time
	time = os.date("*t")

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Variables
local isweekend_variable			= 'Var_IsWeekend'
local isseason_variable				= 'Var_IsSeason'
local isholiday_variable			= 'Var_IsHoliday'
local istimeoffday_variable			= 'Var_IsTimeOfTheDay'
local istemptext_variable			= 'Var_IsTemptxt'
--
-- **********************************************************
-- Script Time Functions Begin
-- **********************************************************
--	

-- Get System Time
	time = os.date("*t")
	
	local m = os.date('%M')
	local weathersensor = 'Temp + Humidity + Baro' -- weatherunderground
	
--
-- **********************************************************
-- Is weekend?
-- **********************************************************
-- weekday [0-6 = Sunday-Saturday]
	function IsWeekend()
		local dayNow = tonumber(os.date("%w"))
		local weekend
		if (dayNow == 0) or (dayNow == 5) or (dayNow == 6) then weekend = 1
		else weekend = 0
		end 
		return weekend
	end

--
-- **********************************************************
-- Is Season
-- **********************************************************
--

-- 0 = Spring
-- 1 = Summer
-- 2 = Autum
-- 3 = Winter

	function WhichSeason()
		local tNow = os.date("*t")
		local dayofyear = tNow.yday
		local season
		if (dayofyear >= 79) and (dayofyear < 172) then season = 0
		elseif (dayofyear >= 172) and (dayofyear < 266) then season = 1
		elseif (dayofyear >= 266) and (dayofyear < 355) then season = 2
		else season = 3
		end
		return season
	end
	
--
-- **********************************************************
-- Time
-- **********************************************************
--

	year 	= tonumber(os.date("%Y"));
	month 	= tonumber(os.date("%m"));
	day 	= tonumber(os.date("%d"));
	hour 	= tonumber(os.date("%H"));
	min 	= tonumber(os.date("%M"));
	weekday = tonumber(os.date("%w"));
	season  = WhichSeason();
	weekend = IsWeekend();	

--
-- **********************************************************
-- Script Time Functions End
-- **********************************************************
--

if (time.hour == 00) and (time.min == 01) then

--
-- **********************************************************
-- Various TimeFrame Data:
-- **********************************************************
--

-- Is Weekend
	if (uservariables[isweekend_variable] ~= weekend) 
	then 
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isweekend_variable..' set to '..weekend..'...</font>')	
		end		
		commandArray["Variable:" .. isweekend_variable .. ""] = tostring(weekend) 
	end

-- Is Season	
	if (uservariables[isseason_variable] ~= season) 
	then
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isseason_variable..' set to '..season..'...</font>')	
		end			
		commandArray["Variable:" .. isseason_variable .. ""] = tostring(season) 
	end
	
--
-- **********************************************************
-- Some National / International Holidays to control some light scenes:
-- **********************************************************
--
isholiday_txt_variable = tostring(uservariables[isholiday_variable])
-- St Maarten	
	if month == 11 and day == 11 and uservariables[isholiday_variable] ~= "St_Maarten" then commandArray["Variable:" ..isholiday_variable.. ""] = "St_Maarten" 
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "St_Maarten"...</font>')	
		end	
-- Sinterklaas
	elseif month == 12 and day == 05 and uservariables[isholiday_variable] ~= "Sinterklaas" then commandArray["Variable:" ..isholiday_variable.. ""] = "Sinterklaas"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "Sinterklaas"...</font>')	
		end	
-- Xmas
	elseif month == 12 and day == 25 and uservariables[isholiday_variable] ~= "Kerstdag" then commandArray["Variable:" ..isholiday_variable.. ""] = "Kerstdag"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "Kerstdag"...</font>')	
		end		
	elseif month == 12 and day == 26 and uservariables[isholiday_variable] ~= "Kerstdag" then commandArray["Variable:" ..isholiday_variable.. ""] = "Kerstdag"	
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "Kerstdag"...</font>')	
		end	
-- New Year
	elseif month == 12 and day == 31 and uservariables[isholiday_variable] ~= "oudjaarsdag" then commandArray["Variable:" ..isholiday_variable.. ""] = "oudjaarsdag"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "Oud_Jaarsdag"...</font>')	
		end		
	elseif month == 01 and day == 01 and uservariables[isholiday_variable] ~= "nieuwjaarsdag" then commandArray["Variable:" ..isholiday_variable.. ""] = "nieuwjaarsdag" 
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "nieuwjaarsdag"...</font>')	
		end	
-- Just a ordinary day
	else
	if uservariables[isholiday_variable] ~= "just_a_normal_day" then
	commandArray["Variable:" ..isholiday_variable.. ""] = "just_a_normal_day" 
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..isholiday_variable..' set from "'..isholiday_txt_variable..'" to "just_a_normal_day"...</font>')	
		end		
	end	
	end	
end

if (m % 30 == 0) then	
--
-- **********************************************************
-- Daytime Data:
-- **********************************************************
--
istimeoffday_txt_variable = tostring(uservariables[istimeoffday_variable])
-- Daytime: Summer
	if timeofday['Daytime'] and time.hour >= 5 and time.hour < 8 
		and uservariables[istimeoffday_variable] ~= 'EarlyMorning' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'EarlyMorning'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "EarlyMorning"...</font>')	
		end			
	end

-- Daytime: Winter
	if timeofday['Nighttime'] and time.hour >= 5 and time.hour < 8 
		and uservariables[istimeoffday_variable] ~= 'EarlyMorning'
	then 
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'EarlyMorning'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "EarlyMorning"...</font>')	
		end				
	end	
	
-- Daytime: Summer		
	if timeofday['Daytime'] and time.hour >= 8 and time.hour < 12 
		and uservariables[istimeoffday_variable] ~= 'Morning' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Morning'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Morning"...</font>')	
		end			
	end
	
-- Daytime: Winter
	if timeofday['Nighttime'] and time.hour >= 8 and time.hour < 12 
		and uservariables[istimeoffday_variable] ~= 'Morning'
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Morning'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Morning"...</font>')	
		end				
	end

-- Daytime: Summer/Winter
	if timeofday['Daytime'] and time.hour >= 12 and time.hour < 18 
		and uservariables[istimeoffday_variable] ~= 'Afternoon' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Afternoon'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Afternoon"...</font>')	
		end			
	end
	
-- Daytime: Summer	
	if timeofday['Daytime'] and time.hour >= 18 and time.hour < 21 
		and uservariables[istimeoffday_variable] ~= 'Evening' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Evening'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Afternoon"...</font>')	
		end			
	end
	
-- Daytime: Winter
	if timeofday['Nighttime'] and time.hour >= 18 and time.hour < 21 
		and uservariables[istimeoffday_variable] ~= 'Evening' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Evening'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Evening"...</font>')	
		end				
	end		

-- Daytime: Summer
	if timeofday['Daytime'] and time.hour >= 21 and time.hour < 23 
		and uservariables[istimeoffday_variable] ~= 'EveningLate' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'EveningLate'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "EveningLate"...</font>')	
		end			
	end
	
-- Daytime: Winter
	if timeofday['Nighttime'] and time.hour >= 21 and time.hour < 23 
		and uservariables[istimeoffday_variable] ~= 'EveningLate' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'EveningLate'
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "EveningLate"...</font>')	
		end			
	end		

-- Daytime: Summer/Winter	
	if timeofday['Nighttime'] and time.hour >= 23 
		and uservariables[istimeoffday_variable] ~= 'Night' 
	then
		commandArray["Variable:" ..istimeoffday_variable.. ""]= 'Night' 
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istimeoffday_variable..' set from "'..istimeoffday_txt_variable..'" to "Night"...</font>')	
		end			
	end		

--
-- **********************************************************
-- Weather Data:
-- **********************************************************
--

istemptext_text_variable = tostring(uservariables[istemptext_variable])
sWeatherTemp, sWeatherHumidity, sWeatherPressure, sWeatherdewPoint = otherdevices_svalues[weathersensor]:match("([^;]+);([^;]+);([^;]+)")
sWeatherTemp = tonumber (sWeatherTemp)
sWeatherHumidity = tonumber (sWeatherHumidity)

if sWeatherTemp <=0 and uservariables[istemptext_variable] ~= "vorst" then
	commandArray["Variable:" ..istemptext_variable.. ""]="vorst"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "vorst"...</font>')	
		end		
	elseif sWeatherTemp >= 1 and sWeatherTemp < 10 and uservariables[istemptext_variable] ~= "koud" then
	commandArray["Variable:" ..istemptext_variable.. ""]="koud"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "koud"...</font>')	
		end		
	elseif sWeatherTemp >= 10 and sWeatherTemp < 15 and uservariables[istemptext_variable] ~= "fris" then
	commandArray["Variable:" ..istemptext_variable.. ""]="fris"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "fris"...</font>')	
		end	
	elseif sWeatherTemp >= 15 and sWeatherTemp < 20 and uservariables[istemptext_variable] ~= "aangenaam" then
	commandArray["Variable:" ..istemptext_variable.. ""]="aangenaam"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "aangenaam"...</font>')	
		end	
	elseif sWeatherTemp >= 20 and sWeatherTemp < 25 and uservariables[istemptext_variable] ~= "warm" then
	commandArray["Variable:" ..istemptext_variable.. ""]="warm"
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "warm"...</font>')	
		end	
	elseif sWeatherTemp >= 25 and uservariables[istemptext_variable] ~= "heet" then
		if uservariables[istemptext_variable] ~= "heet" then
		commandArray["Variable:" ..istemptext_variable.. ""]="heet"	
		if verbose == 1 then	
		print('<font color="Red">-- Variables Script ==> '..istemptext_variable..' set from "'..istemptext_text_variable..'" to "heet"...</font>')	
		end			
		end
end	
end
	