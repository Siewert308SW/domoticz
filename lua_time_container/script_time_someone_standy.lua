--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_someonehome_standby.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch OFF dummy SomeOneHomeStandby which is triggerd when someoneHome is OFF to prevent triggering Someonehome again 
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- SomeOneHome Standby Settings
-- **********************************************************
--

local nobody_home							= 'Niemand Thuis'
local someonehome_standby_nomotion_counter 	= 'IsSomeonehome_Standby_NoMotionCounter'
local someonehome							= 'Iemand Thuis'
local someonehome_standby					= 'Iemand Thuis - Standby'
local someonehome_standby_nomotion_timeout	= 5 --(1 minute counter)

--
-- **********************************************************
-- SomeOneHome Standby No Motion Counter
-- **********************************************************
--

someonehome_standby_motion_minutes = tonumber(uservariables[someonehome_standby_nomotion_counter])
someonehome_standby_remaining = someonehome_standby_nomotion_timeout - someonehome_standby_motion_minutes

if (otherdevices[someonehome_standby] == 'On' and otherdevices[someonehome] == 'Off') then
	someonehome_standby_motion_minutes = someonehome_standby_motion_minutes + 1

if verbose == 1 then	
	print('<font color="blue">-- '..someonehome_standby..' ==> '..someonehome_standby..' On for ' ..tonumber(someonehome_standby_motion_minutes).. ' minutes, '..someonehome_standby..' will be switched OFF in aprox ' ..tonumber(someonehome_standby_remaining).. ' minutes!</font>')	
end
	
	commandArray['Variable:' .. someonehome_standby_nomotion_counter] = tostring(someonehome_standby_motion_minutes)
end 

----------

if otherdevices[someonehome] == 'Off' and otherdevices[someonehome_standby] == 'On' and someonehome_standby_motion_minutes > tonumber(someonehome_standby_nomotion_timeout) then
 	commandArray[someonehome_standby]='Off'

if verbose == 1 then	
	print('<font color="blue">-- '..someonehome_standby..' ==> Still no one home after ' ..tonumber(someonehome_standby_motion_minutes).. ' minutes, '..someonehome_standby..' has been be switched OFF!</font>')	
end
	
	someonehome_standby_motion_minutes = 0
	commandArray['Variable:' .. someonehome_standby_nomotion_counter] = tostring(someonehome_standby_motion_minutes)	
end

----------

if otherdevices[nobody_home] == 'On' and otherdevices[someonehome] == 'Off' and otherdevices[someonehome_standby] == 'Off' and someonehome_standby_motion_minutes > 0 then
	
if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..someonehome_standby..' ==> Counter has been reset!</font>')	
end
	
	someonehome_standby_motion_minutes = 0
	commandArray['Variable:' .. someonehome_standby_nomotion_counter] = tostring(someonehome_standby_motion_minutes)	
end

----------

if otherdevices[someonehome] == 'On' and someonehome_standby_motion_minutes > 0 then

if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..someonehome_standby..' ==> '..someonehome..' activated, Counter has been reset!</font>')	
end
	
	someonehome_standby_motion_minutes = 0
	commandArray['Variable:' .. someonehome_standby_nomotion_counter] = tostring(someonehome_standby_motion_minutes)	
end

----------

if otherdevices[someonehome_standby] == 'Off' and someonehome_standby_motion_minutes > 0 then

if verbose == 1 then	
	print('<font color="blue"><font color="blue">-- '..someonehome_standby..' ==> '..someonehome..' deactivated, Counter has been reset!</font>')	
end
	
	someonehome_standby_motion_minutes = 0
	commandArray['Variable:' .. someonehome_standby_nomotion_counter] = tostring(someonehome_standby_motion_minutes)	
end