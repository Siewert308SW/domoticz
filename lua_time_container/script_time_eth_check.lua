--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_eth_check.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-10-2017
	@Script to check if eth0 ip has been changed while running headless
	@Why...? My current modem provided by my cable provider sucks sucks when it comes to static adresses
	@Its registered by MAC and don't have the time to go true all the hazzle for a installing a new one
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = All
local verbose = 1

local my_ip			= '192.168.178.13'
local my_ip_notify	= 'IsEth_State'


subject = "Domoticz - eth ip changed!" --Subject of the email notification
body = "Network ip changed: " --Body text of the email/SMS/notification. Name of the door which was opened will be added at the end of body text by the script.

-- Get System Time
	time = os.date("*t")
	
	local m = os.date('%M')

if (m % 60 == 0) then -- Check every hour
	
--commandArray = {}
 
	f = assert (io.popen ("ifconfig eth0 | grep 'inet addr'| awk '{print $2}' | cut -d ':' -f 2"))
	 
	for line in f:lines() do
	 
	-- Sending 1 email when local ip has been changed 
		if line ~= my_ip and uservariables[my_ip_notify] == 0 then
		 
		if verbose == 1 then	
		print('<font color="darkred">-- eth0 IP ==> IP has been changed to '..my_ip..', Sending alert mail...</font>')	
		end
		bodytext = ''..body.."from "..my_ip.." to "..line..''
		commandArray["Variable:" .. my_ip_notify .. ""]= '1'
		commandArray['SendNotification']=subject.."#"..bodytext.."#0"	
		 
		end
		
	-- Reset variable 
		if line == my_ip and uservariables[my_ip_notify] == 1 then
		 
		if verbose == 1 then	
		print('<font color="darkred">-- eth0 IP ==> IP local variable has been corrected...</font>')	
		end
		commandArray["Variable:" .. my_ip_notify .. ""]= '0' 
		end	
	end
end 
--return commandArray