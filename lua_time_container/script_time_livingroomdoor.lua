--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_livingroom_door.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to virtual close KaKu/CoCo door sensor if signal missed so a new event can be triggered anyways
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Livingroom Door Settings
-- **********************************************************
--	

local livingroom_door					= 'Kamer Deur'
local livingroom_door_nomotion_counter 	= 'IsLivingroomDoor_NoMotionCounter'
local livingroom_door_nomotion_timeout	= 10 --(1 minute counter)


--
-- **********************************************************
-- Livingroom Door No Motion Counter
-- **********************************************************
--

livingroom_door_motion_minutes = tonumber(uservariables[livingroom_door_nomotion_counter])
livingroom_door_remaining = livingroom_door_nomotion_timeout - livingroom_door_motion_minutes

if (otherdevices[livingroom_door] == 'Open') then
	livingroom_door_motion_minutes = livingroom_door_motion_minutes + 1
		if verbose == 1 then	
		print('<font color="blue">-- '..livingroom_door..' ==> '..livingroom_door..' open, It will be virtually closed in about ' ..tonumber(livingroom_door_remaining).. ' minutes...</font>')	
		end
	commandArray['Variable:' .. livingroom_door_nomotion_counter] = tostring(livingroom_door_motion_minutes)
end 

----------

if otherdevices[livingroom_door] == 'Open' and livingroom_door_motion_minutes > tonumber(livingroom_door_nomotion_timeout) then
 	commandArray[livingroom_door]='Off'
		if verbose == 1 then	
		print('<font color="blue">-- '..livingroom_door..' ==> '..livingroom_door..' still open, It will be virtually closed now...</font>')	
		end
	livingroom_door_motion_minutes = 0
	commandArray['Variable:' .. livingroom_door_nomotion_counter] = tostring(livingroom_door_motion_minutes)	
end

----------

if otherdevices[livingroom_door] == 'Closed' and livingroom_door_motion_minutes > 0 then
		if verbose == 1 then	
		print('<font color="blue">-- '..livingroom_door..' ==> '..livingroom_door..' closed, Counter has been reset...</font>')	
		end
	livingroom_door_motion_minutes = 0
	commandArray['Variable:' .. livingroom_door_nomotion_counter] = tostring(livingroom_door_motion_minutes)	
end

	