--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_toilet_light.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 26-12-2016
	@Time script to switch OFF toilet light if forgotten to turn OFF manually or missed KaKu/CoCo signal
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Toilet Settings
-- **********************************************************
--	

local toilet_motion_detector		= 'W.C Motion'
local toilet_nomotion_counter 		= 'IsToilet_NoMotionCounter'
local toilet_light					= 'W.C Lamp'
local toilet_nomotion_timeout		= 15 --(1 minute counter)

--
-- **********************************************************
-- Toilet No Motion Counter
-- **********************************************************
--

toilet_motion_minutes = tonumber(uservariables[toilet_nomotion_counter])
toilet_remaining = toilet_nomotion_timeout - toilet_motion_minutes

if (otherdevices[toilet_motion_detector] == 'Off' and otherdevices[toilet_light] == 'On') then
	toilet_motion_minutes = toilet_motion_minutes + 1

		if verbose == 1 then	
			print('<font color="Brown">-- '..toilet_light..' ==> '..toilet_light..' ON, It will be switched OFF in aprox ' ..tonumber(toilet_remaining).. ' minutes...</font>')	
		end
	commandArray['Variable:' .. toilet_nomotion_counter] = tostring(toilet_motion_minutes)
end 

----------

if otherdevices[toilet_light] == 'On' and toilet_motion_minutes > tonumber(toilet_nomotion_timeout) then
 	commandArray[toilet_light]='Off'
	
		if verbose == 1 then	
			print('<font color="Brown"><font color="Brown">-- '..toilet_light..' ==> Nobody seen for ' ..tonumber(toilet_motion_minutes).. ' minutes, '..toilet_light..' switched OFF</font>')	
		end
	toilet_motion_minutes = 0
	commandArray['Variable:' .. toilet_nomotion_counter] = tostring(toilet_motion_minutes)	
end

----------

if otherdevices[toilet_light] == 'Off' and toilet_motion_minutes > 0 and toilet_motion_minutes < tonumber(toilet_nomotion_timeout) then
	
		if verbose == 1 then	
			print('<font color="Brown"><font color="Brown">-- '..toilet_light..' ==> '..toilet_light..' switched OFF manually before reaching max runtime, Counter has been reset!</font>')	
		end
	toilet_motion_minutes = 0
	commandArray['Variable:' .. toilet_nomotion_counter] = tostring(toilet_motion_minutes)	
end

----------

if otherdevices[toilet_motion_detector] == 'On' and otherdevices[toilet_light] == 'On' and toilet_motion_minutes < tonumber(toilet_nomotion_timeout) then
	
		if verbose == 1 then	
			print('<font color="Brown"><font color="Brown">-- '..toilet_light..' ==> Motion detected, counter has been reset!</font>')	
		end
	toilet_motion_minutes = 0
	commandArray['Variable:' .. toilet_nomotion_counter] = tostring(toilet_motion_minutes)	
end
	