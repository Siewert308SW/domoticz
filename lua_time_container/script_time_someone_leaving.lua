--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_someone_leaving.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Time script to switch OFF dummy when somebody left home and turn OFF garden lights if switched ON (see script_device_someone_leaving.lua)
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

--
-- **********************************************************
-- Leaving Settings
-- **********************************************************
--	

local leaving_standby				= 'Vertrek - Standby'
local leaving_counter 				= 'IsLeaving_NoMotionCounter'
local frontdoor						= 'Voor Deur'
local backdoor			 			= 'Achter Deur'
local sliding_door	 				= 'Schuifpui'
local temptxt			 			= 'Var_IsTemptxt'

--
-- **********************************************************
-- Garden lights timeout depending on the outside temperature
-- **********************************************************
--

	if uservariables["Var_IsTemptxt"] == "vorst" then
	leaving_counter_timeout = 10 --(1 minute counter)	
	elseif uservariables["Var_IsTemptxt"] == "koud" then
	leaving_counter_timeout = 5 --(1 minute counter)	
	elseif uservariables["Var_IsTemptxt"] == "fris" then
	leaving_counter_timeout = 5 --(1 minute counter)	
	elseif uservariables["Var_IsTemptxt"] == "lekker" then
	leaving_counter_timeout = 2 --(1 minute counter)
	elseif uservariables["Var_IsTemptxt"] == "warm" then
	leaving_counter_timeout = 2 --(1 minute counter)
	elseif uservariables["Var_IsTemptxt"] == "heet" then
	leaving_counter_timeout = 2 --(1 minute counter)
	else	
	leaving_counter_timeout = 1 --(1 minute counter)	
	end

--
-- **********************************************************
-- Leaving No Motion Counter
-- **********************************************************
--

leaving_counter_minutes = tonumber(uservariables[leaving_counter])
leaving_remaining = leaving_counter_timeout - leaving_counter_minutes

if (otherdevices[leaving_standby] == 'On' and otherdevices[frontdoor] == 'Closed' and otherdevices[backdoor] == 'Closed' and otherdevices[sliding_door] == 'Closed') then
	leaving_counter_minutes = leaving_counter_minutes + 1

		if verbose == 1 then		
		print('<font color="blue">-- '..leaving_standby..' ==> Some one left ' ..tonumber(leaving_counter_minutes).. ' minutes ago, In aprox ' ..tonumber(leaving_remaining).. ' minutes then garden lights will be switched OFF!</font>')	
		end

	commandArray['Variable:' .. leaving_counter] = tostring(leaving_counter_minutes)
end 

----------

if otherdevices[leaving_standby] == 'On' and leaving_counter_minutes > tonumber(leaving_counter_timeout) then
 	commandArray[leaving_standby]='Off'

		if verbose == 1 then		
			print('<font color="blue"><font color="blue">-- '..leaving_standby..' ==> Some one left ' ..tonumber(leaving_counter_minutes).. ' minutes ago, garden lights switched OFF</font>')	
		end
	
	leaving_counter_minutes = 0
	commandArray['Variable:' .. leaving_counter] = tostring(leaving_counter_minutes)	
end

----------

if otherdevices[leaving_standby] == 'Off' and leaving_counter_minutes > 0 then
	
		if verbose == 1 then		
			print('<font color="blue"><font color="blue">-- '..leaving_standby..' ==> Some one left before reaching max runtime, Counter has been reset!</font>')	
		end	
	
	leaving_counter_minutes = 0
	commandArray['Variable:' .. leaving_counter] = tostring(leaving_counter_minutes)	
end
	