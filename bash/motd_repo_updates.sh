#!/bin/bash

#######################################################################################################################################################	

### motd_updates.sh
### @author	: Siewert Lameijer
### @since	: 29-11-2016
### @updated: 26-12-2016
### Script to check for rpi, repo updated packages to display in motd

#######################################################################################################################################################	

sudo apt-get update
sudo apt-get upgrade -d -y | grep 'upgraded,' | awk {'print $1'} > /mnt/storage/domoticz_scripts/logging/motd_repo_updates/repo_updates.txt 
sudo apt-get install rpi-update -d -y | grep 'upgraded,' | awk {'print $1'} > /mnt/storage/domoticz_scripts/logging/motd_repo_updates/rpi_updates.txt
echo "Update Check Complete"

exit
