#!/bin/bash

#######################################################################################################################################################	

### pikill.sh
### @author	: Siewert Lameijer
### @since	: 29-11-2016
### @updated: 1-8-2017
### RPi shutdown script to overcome UPS PIco HV3.0A shutdown issue

#######################################################################################################################################################	
sudo service domoticz.sh stop
sleep 10
i2cset -y 1 0x6B 0x00 0xcc
