#!/bin/bash

#######################################################################################################################################################	

### reboot_disable_hardware.sh
### @author	: Siewert Lameijer
### @since	: 29-11-2016
### @updated: 01-13-2016
### Script to disable some hardware at reboot

#######################################################################################################################################################	
sudo /opt/vc/bin/tvservice -o
sleep 1
sudo ifconfig wlan0 down
sleep 1
sudo ifdown wlan0
exit
