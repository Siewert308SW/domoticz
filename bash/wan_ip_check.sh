#!/bin/bash

#######################################################################################################################################################	

### wan_ip_check.sh
### @author	: Ray Chan
### @since	: 29-11-2016
### @updated: 26-12-2016
### Simple script to send alert for ip changes
### http://nas.deadcode.net/2009/07/26/synology-email-alert-when-external-ip-changed/2/

#######################################################################################################################################################	


# If you want a simple log file, assign 1 to log_enabled, otherwise set it to 0
log_enabled=1; #0=disable, 1=enable

# date format used by log file
datestamp=`date '+%d-%m-%Y %H:%M'`

# the actual command getting the public IP
myipnow=`wget -4 -qO - icanhazip.com` 

# Your WAN IP
previp="<Your WAN IP>";

# path of the log file, ignore if log_enabled=0
logfile="/mnt/storage/domoticz_scripts/logging/wan_ip_check/wan_ip_check.log"

# path of the temporary file storing previous ip address
iplog="/mnt/storage/domoticz_scripts/logging/wan_ip_check/wan_ip_check.txt"

file="/mnt/storage/domoticz_scripts/logging/mail_concepts/wan_ip_check_mail_concept.txt"
mailfile="/mnt/storage/domoticz_scripts/logging/wan_ip_check/mail.txt"

if [ -f $iplog ]; then
   previp=`cat $iplog`
fi

if [ "$myipnow" != "$previp" ]; then

	cp $file $mailfile
	
   if [ -f $mailfile ]; then
      printf "\nJe provider heeft het IP van je modem veranderd\nIP is van $previp naar $myipnow gewijzigd\n\nBye!" >> $mailfile
   fi
   
   #ip changed, sending alert
curl --url "<smpt.server.com>:25" --ssl \
  --mail-from "<info@mailtosentto.com>" --mail-rcpt "<info@mailfrom.com>" \
  --upload-file $mailfile

     #Remove mail file
   
	if [ -f $mailfile ] ; then
		rm $mailfile		
	fi  
	
   #write the new ip to log file
   echo $myipnow > $iplog
   if [ $log_enabled = 1 ]; then
      echo "$datestamp IP changed, sending notification email. $previp | $myipnow" >> $logfile
   fi
else
   if [ $log_enabled = 1 ]; then
      echo "$datestamp IP is same, skipping notification. $previp | $myipnow" >> $logfile
   fi 
fi

exit
