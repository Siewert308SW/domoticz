I stopped pushing files here.
And moved my repository to github...
https://github.com/Siewert308SW

### Event Scripts For Domoticz ###

Over here you'll find my Lua, bash, Python, motd scripts
Which i want to share with the WWW so it can be a guide or maybe to get you new inspiration.  
You'll notice that all most all my scripts have a double On or OFF command.  
As my entire system is based on KaKu/CoCo modules.  
By using a double command i overrule the possibility of a missed signal.  
It aint ideal but for now it works fine.  

In general i won't provide any install guides and such.
I assume you have some basic knowledge about Raspberry, Domoticz and scripting.  
Most Lua scripts contain header at every event which explains what is going on, if not... #oops ;-)  

### What is this repository for? ###

* Various Lua event and timer scripts for Domoticz
* Various Bash scripts 
* Various Python script

### Different Lua usage? ###

I have a different Lua script usage.  
Normally you have all your Lua scripts in /home/pi/domoticz/scripts/lua/*  
Back in the Pi2 days all those scripts contained a commandArray = {} & return commandArray which took a lot of CPU usage as all those scripts where opened and closed.

I took a different path.  
I created one script in the /lua folder and called it script_device_container.lua  
In this script i set all my devices and switches who should trigger a event script.  
Then i created a folder outside the /lua folder and called it lua_container.  
In this folder i dumped my Lua event scripts as we know.  
Only thing these script don't contain are the commandArray = {} & return commandArray  
These scripts are called by a dofile command in script_device_container.lua  

This way it saves a lot of commandArray = {} & return commandArray and there for CPU Usage.  
Back in my Pi2 days i had a CPU USage of 80/90% but after converting to this method i decreased to 10%  
The commandArray = {} & return commandArray are handled by the script_device_container.  

**Example of my motd:**   
![motd_new.png](https://bitbucket.org/repo/E5MpGM/images/3202926430-motd_new.png)