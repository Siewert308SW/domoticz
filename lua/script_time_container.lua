--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_time_container.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 25-12-2016
	@Large device switches container to trigger lua event scripts if needed
	
	The idea behind this different Lua usage is to save resources and gain faster device reponse times.
	Normally each script uses a "commandArray = {}" & "return commandArray" which i removed from my Lua event scripts
	As this container handles those entries and saves resources.
	By moving all your scripts outside Domoticz and call them when necessary increases response times.
	Down below you will find a code which searches your time scripts in your desired folder and exectues them.
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

--
-- **********************************************************
-- Container Settings Begin
-- **********************************************************
--

-- Get System Time
	time = os.date("*t")

-- Set Lua Path
	lua_path = "/home/pi/domoticz/scripts/lua_time_container/"	
	
--
-- **********************************************************
-- Container Settings End
-- **********************************************************
--

commandArray = {}
f = io.popen('ls ' .. lua_path)
for name in f:lines() do
dofile ('/home/pi/domoticz/scripts/lua_time_container/'..name..'')
end
return commandArray
	