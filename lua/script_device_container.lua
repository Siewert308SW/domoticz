--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_container.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-19-2017
	@Large device switches container to trigger lua event scripts if needed
	
	The idea behind this different Lua usage is to save resources and gain faster device reponse times.
	Normally each script uses a "commandArray = {}" & "return commandArray" which i removed from my Lua event scripts
	As this container handles those entries and saves resources.
	By moving all your scripts outside Domoticz and call them when necessary increases response times.
	Down below you will find all devices in my setup which triggers a scene or event and calls a Lua script responding to that trigger...
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

--
-- **********************************************************
-- Container Settings Begin
-- **********************************************************
--

-- Get System Time
	time = os.date("*t")

-- Set Lua Path
	lua_path = "/home/pi/domoticz/scripts/lua_device_container/"
	
--	Portable Devices												 -- Portable Devices
	local phone_1 						= 'Jerina GSM'					local laptop_1 						= 'Jerina Laptop'
	local phone_2 						= 'Siewert GSM'					local laptop_2 						= 'Siewert Laptop'
	local phone_3 						= 'Natalya GSM'					local laptop_3 						= 'Natalya Laptop'
	local phone_switch 					= 'Telefoons'					local laptop_switch 				= 'Laptops'

-- Network Devices													 -- Media Devices
	local internet 						= 'Ethernet'					local television 					= 'Televisie'
	local router 						= 'Router'						local mediabox 						= 'MediaBox'
	local network 						= 'Netwerk'						local media_switch 					= 'Media'

-- Door/Window Sensors												 -- IsDark Switches
	local frontdoor						= 'Voor Deur'					local isdark_front_border_trigger 	= 'IsDonker_Border_Verlichting'
	local backdoor			 			= 'Achter Deur'					local isdark_garden_lights_trigger 	= 'IsDonker_Tuin_Verlichting'
	local livingroom_door				= 'Kamer Deur'					local isdark_dinner_table 			= 'IsDonker_Eettafel'
	local sliding_door	 				= 'Schuifpui'					local isdark_living_room_trigger_1	= 'IsDonker_Woonkamer_1'
	local scullery_door 				= 'Bijkeuken Deur'				local isdark_living_room_trigger_2	= 'IsDonker_Woonkamer_2'
	local motion_upstairs 				= 'Trap Motion Boven'			local isdark_standby				= 'IsDonker - Standby'
	local motion_downstairs 			= 'Trap Motion Beneden' 		local isdark_sunset					= 'Sunrise/Sunset'
	local toilet_motion_detector		= 'W.C Motion'					local isdark_xmas_lights 			= 'IsDonker_Kerst_Verlichting'

-- SomeOneHome Triggers												 -- Light Switches
	local someonehome					= 'Iemand Thuis'				local livingroom_light_switch		= 'Woonkamer Verlichting Knop'
	local someonehome_standby			= 'Iemand Thuis - Standby'		local dinnertable_light_switch		= 'Woonkamer Eettafel Verlichting Knop'
	local arrival_standby				= 'Aankomst - Standby'			local garden_light_switch			= 'Tuin Verlichting Knop'
	local nobody_home					= 'Niemand Thuis'				local toilet_light					= 'W.C Lamp'
	local nest_away						= 'Nest Away'					local shower_light					= 'Douche Lamp'
	local dinner_table_motion 			= 'Motion Eettafel'

-- Various Standby Triggers											 -- Various Switches
	local leaving_standby				= 'Vertrek - Standby'			local doorbell 						= 'Deurbel'
	local doorbell_standby 				= 'Deurbel - Standby'			local pico_power    				= 'PIco RPi Powered'
	local upstairs_standby 				= 'Trap Boven - Standby'		local pico_voltage 					= 'PIco BAT Voltage'
	local downstairs_standby 			= 'Trap Beneden - Standby'
	local walktrue_standby 				= 'Doorloop - Standby'	
	
--
-- **********************************************************
-- Container Settings End
-- **********************************************************
--

commandArray = {}

--
-- **********************************************************
-- SomeOneHome Triggers
-- **********************************************************
--

	if devicechanged[someonehome]		
	then
		dofile(lua_path.."script_device_lights_livingroom.lua")	
		dofile(lua_path.."script_device_lights_livingroom_xmas.lua")
		dofile(lua_path.."script_device_lights_dinnertable.lua")	
		dofile(lua_path.."script_device_standby_killers.lua") --included power outage restore
		dofile(lua_path.."script_device_lights_garden_shed.lua") --included power outage restore
		dofile(lua_path.."script_device_lights_garden_border.lua") --included power outage restore
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua") --included power outage restore		
	end
	
	if devicechanged[someonehome_standby]		
	then
		dofile(lua_path.."script_device_lights_livingroom_away.lua")		
		dofile(lua_path.."script_device_lights_livingroom_away_xmas.lua")
		dofile(lua_path.."script_device_lights_garden_shed.lua")	
		dofile(lua_path.."script_device_lights_garden_border.lua")
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua")			
	end	
	
	if devicechanged[nobody_home]		
	then
		dofile(lua_path.."script_device_standby_killers.lua")	
	end	
	
	if devicechanged[pico_voltage] then
		dofile(lua_path.."script_device_ups_pico_fan_control.lua")
		dofile(lua_path.."script_device_ups_pico_power_control.lua")		
	end
	
	if devicechanged[pico_power] then
		dofile(lua_path.."script_device_ups_pico_power_control.lua")
		dofile(lua_path.."script_device_someone_home.lua")		
	end	

--
-- **********************************************************
-- Portable Devices
-- **********************************************************
--

	if devicechanged[phone_1]
		or devicechanged[phone_2]
		or devicechanged[phone_3]		
	then
		dofile(lua_path.."script_device_switch_phones.lua")
		dofile(lua_path.."script_device_someone_leaving.lua")
	end
	
	if devicechanged[phone_switch]		
	then
		dofile(lua_path.."script_device_someone_home.lua")		
	end	

--
-- **********************************************************
-- Laptop Devices
-- **********************************************************
--
	
	if devicechanged[laptop_1]
		or devicechanged[laptop_2]
		or devicechanged[laptop_3]		
	then
		dofile(lua_path.."script_device_switch_laptops.lua")	
	end

	if devicechanged[laptop_switch]	
		or devicechanged[dinner_table_motion]
	then
		dofile(lua_path.."script_device_someone_home.lua")
		dofile(lua_path.."script_device_lights_dinnertable.lua")		
	end	

--
-- **********************************************************
-- Network Devices
-- **********************************************************
--

	if devicechanged[internet]
		or devicechanged[router]		
	then
		dofile(lua_path.."script_device_switch_network.lua")		
	end	
	
--
-- **********************************************************
-- Media Devices
-- **********************************************************
--

	if devicechanged[television]
		or devicechanged[mediabox]		
	then
		dofile(lua_path.."script_device_someone_home.lua")	
		dofile(lua_path.."script_device_switch_media.lua")	
		dofile(lua_path.."script_device_standby_killers.lua")		
	end

--
-- **********************************************************
-- Door/Window and motion Sensors
-- **********************************************************
--	
	
	if devicechanged[toilet_motion_detector]		
	then
		dofile(lua_path.."script_device_activity_toilet.lua")		
	end

	if devicechanged[frontdoor]
		or devicechanged[backdoor]
		or devicechanged[livingroom_door]
		or devicechanged[sliding_door]
		or devicechanged[scullery_door]
		or devicechanged[motion_upstairs]
		or devicechanged[motion_downstairs]		
	then
		--dofile(lua_path.."script_device_activity_stairs.lua")	
		dofile(lua_path.."script_device_someone_home.lua")
		dofile(lua_path.."script_device_someone_leaving.lua")
		dofile(lua_path.."script_device_activity_shower.lua")
		dofile(lua_path.."script_device_activity_shower_xmas.lua")	
		dofile(lua_path.."script_device_activity_doorbell.lua")			
	end	
	
--
-- **********************************************************
-- IsDark Triggers
-- **********************************************************
--
	
	if devicechanged[isdark_front_border_trigger]
		or devicechanged[isdark_garden_lights_trigger]
		or devicechanged[isdark_living_room_trigger_1]
		or devicechanged[isdark_living_room_trigger_2]
		or devicechanged[isdark_dinner_table]		
		or devicechanged[isdark_standby]
		or devicechanged[isdark_sunset]
		or devicechanged[isdark_xmas_lights]		
	then
		dofile(lua_path.."script_device_lights_livingroom.lua")	
		dofile(lua_path.."script_device_lights_livingroom_xmas.lua")
		dofile(lua_path.."script_device_lights_livingroom_away.lua")		
		dofile(lua_path.."script_device_lights_livingroom_away_xmas.lua")		
		dofile(lua_path.."script_device_lights_dinnertable.lua")
		dofile(lua_path.."script_device_lights_garden_shed.lua")	
		dofile(lua_path.."script_device_lights_garden_border.lua")
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua")			
	end	

--
-- **********************************************************
-- Light Switches
-- **********************************************************
--
	if devicechanged[livingroom_light_switch]
	then
		dofile(lua_path.."script_device_lights_livingroom.lua")	
		dofile(lua_path.."script_device_lights_livingroom_xmas.lua")
	end	
	
	if devicechanged[dinnertable_light_switch]
	then
		dofile(lua_path.."script_device_lights_dinnertable.lua")
	end	

	if devicechanged[toilet_light]
	then	
		dofile(lua_path.."script_device_activity_toilet.lua")
	end
	
	if devicechanged[shower_light]
	then	
		dofile(lua_path.."script_device_activity_shower.lua")
		dofile(lua_path.."script_device_activity_shower_xmas.lua")		
	end	
--
--
-- **********************************************************
-- Various Standby Triggers
-- **********************************************************
--

	if devicechanged[leaving_standby]
	then
		dofile(lua_path.."script_device_someone_leaving.lua")		
	end	
	
--
--
-- **********************************************************
-- Doorbell Triggers
-- **********************************************************
--

	if devicechanged[doorbell]
	then	
		dofile(lua_path.."script_device_activity_doorbell.lua")	
	end
	
	if devicechanged[doorbell_standby]
	then	
		dofile(lua_path.."script_device_activity_doorbell.lua")	
	end	
--[[	
	if devicechanged[upstairs_standby]
	or devicechanged[downstairs_standby]
	then	
		--dofile(lua_path.."script_device_activity_stairs.lua")
	end	
]]	
--
--
-- **********************************************************
-- Time Triggers
-- **********************************************************
--

-- Check if someone is home and set standby variable so if anybody has gone after this time garden lights will switch OFF	
	if (time.hour == 22) and (time.min == 15) then
		dofile(lua_path.."script_device_lights_garden_border.lua")
		dofile(lua_path.."script_device_lights_garden_shed.lua")
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua")		
	end

-- Switch Twilight scene OFF when nobody home and IsNotWeekend	
	if (time.hour == 22) and (time.min == 45) then
		dofile(lua_path.."script_device_lights_livingroom_away.lua")		
		dofile(lua_path.."script_device_lights_livingroom_away_xmas.lua")		
	end

-- Switch Garden lights OFF when IsNotWeekend and someonehome	
	if (time.hour == 22) and (time.min == 50) then
		dofile(lua_path.."script_device_lights_garden_shed.lua")	
		dofile(lua_path.."script_device_lights_garden_border.lua")
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua")		
	end

-- Switch Twilight scene OFF when nobody home and IsWeekend	
	if (time.hour == 23) and (time.min == 15) then
		dofile(lua_path.."script_device_lights_livingroom_away.lua")		
		dofile(lua_path.."script_device_lights_livingroom_away_xmas.lua")
	end	

-- Switch Garden lights OFF when nobody home and IsWeekend
	if (time.hour == 23) and (time.min == 30) then
		dofile(lua_path.."script_device_lights_garden_shed.lua")	
		dofile(lua_path.."script_device_lights_garden_border.lua")
		dofile(lua_path.."script_device_lights_garden_border_xmas.lua")		
	end

return commandArray