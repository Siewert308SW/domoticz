#!/bin/bash

# File Location: /home/pi/

let upSeconds="$(/usr/bin/cut -d. -f1 /proc/uptime)"
let secs=$((${upSeconds}%60))
let mins=$((${upSeconds}/60%60))
let hours=$((${upSeconds}/3600%24))
let days=$((${upSeconds}/86400))
UPTIME=`printf "%d days, %02d:%02d:%02d" "$days" "$hours" "$mins" "$secs"`

let PSU=`ps U pi h | wc -l`
let PSA=`ps -A h | wc -l`

ME=$(whoami)

HOMEAUTOMATIONSERVICE=`sudo service domoticz.sh status | grep GizMoCuz | awk {'print $8,$9'}`
HOMEAUTOMATIONHASH=`sudo service domoticz.sh status | grep Build | awk {'print $10,$11,$12'}`
RELEASE=`/usr/bin/lsb_release -s -d  | grep Raspbian | awk {'print $1,$3,$4'}`
KERNEL=`uname -srm`
MODEL=`cat /proc/device-tree/model`
CPUTEMP=`vcgencmd measure_temp | /bin/grep "temp" | /usr/bin/cut -d "=" -f 2 | /usr/bin/cut -d " " -f 1`
CPUMHZ=$(sudo cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq)

HDD1=`df -h | grep 'dev/root' | awk '{print $2," 	" $3, "	    "$4}'`
HDD2=`df -h | grep 'mnt/storage' | awk '{print $2," 	 " $3, "	    "$4}'`
MEM=`free -mo | awk 'NR==2 { printf "%sM 	 %sM 	    %sM ",$2,$3,$4; }'`
SWAP=`free -mo | awk 'NR==3 { printf " %sM 	   %sM 	     %sM ",$2,$3,$4; }'`

WLANIP=`/sbin/ifconfig wlan0 | /bin/grep "inet addr" | /usr/bin/cut -d ":" -f 2 | /usr/bin/cut -d " " -f 1`
LANIP=`/sbin/ifconfig eth0 | /bin/grep "inet addr" | /usr/bin/cut -d ":" -f 2 | /usr/bin/cut -d " " -f 1`
WANIP=`wget -q -O - http://icanhazip.com/ | tail`

BTSERVICE=`sudo systemctl status bluetooth -l | grep Active | awk {'print $2'}`
WLANSERVICE=`sudo service wlan status | grep Active | awk {'print $2'}`

DOMOSERVICE=`sudo service domoticz.sh status | grep Active | awk {'print $2'}`
DOMOSERVICECHANNEL=`curl -s -X GET "http://127.0.0.1:8080/json.htm?type=command&param=getversion" |grep -Po '(?<=channel=)[^&]*'`
DOMOSERVICEUPTIME=`sudo service domoticz.sh status | grep Active | awk {'print $9,$10,$11'}`
REPOUPDATESAVAIL=`cat /mnt/storage/domoticz_scripts/logging/motd_repo_updates/repo_updates.txt`
RPIUPDATESAVAIL=`cat /mnt/storage/domoticz_scripts/logging/motd_repo_updates/rpi_updates.txt`
DOMOUPDATESAVAIL=`cat /mnt/storage/domoticz_scripts/logging/motd_domo_updates/domo_updates.txt`
DOMOUPDATEFROM=`cat /mnt/storage/domoticz_scripts/logging/motd_domo_updates/domo_update_from.txt`
DOMOUPDATETO=`cat /mnt/storage/domoticz_scripts/logging/motd_domo_updates/domo_update_to.txt`

# get the load averages
read one five fifteen rest < /proc/loadavg

echo "
$(tput setaf 2)
       .~~.   .~~.
      '. \ ' ' / .'$(tput setaf 1)													$(tput setaf 1)
      : .~.'~'.~. :   $(tput setaf 4)   ____                        _   _          	$(tput setaf 1)
      : .~.'~'.~. :   $(tput setaf 4)	|  _ \  ___  _ __ ___   ___ | |_(_) ___ ____	$(tput setaf 1)
     ~ (   ) (   ) ~  $(tput setaf 4)  | | | |/ _ \|  _   _ \ / _ \| __| |/ __|_  /	$(tput setaf 1)
    ( : '~'.~.'~' : ) $(tput setaf 4)  | |_| | (_) | | | | | | (_) | |_| | (__ / / 	$(tput setaf 1)
     ~ .~ (   ) ~. ~  $(tput setaf 4)	|____/ \___/|_| |_| |_|\___/ \__|_|\___/___|	$(tput setaf 1)
      (  : '~' :  )   $(tput setaf 2)               Welcome back, ${ME}!           	$(tput setaf 1)
       '~ .~~~. ~'    $(tput setaf 2)  	  Outside it's `curl -s "http://rss.accuweather.com/rss/liveweather_rss.asp?metric=1&locCode=EUR|NL|9974|ZOUTKAMP|" | sed -n '/Currently:/ s/.*: \(.*\): \([0-9]*\)\([CF]\).*/\2 \3 and \1/p'` 			                        $(tput setaf 1)
           '~'                                          
$(tput sgr0)
$(tput setaf 7)
	$(tput setaf 2)HOME AUTOMATION$(tput sgr0)
	Pi Model...........: ${MODEL}		
	Domoticz Version...: ${HOMEAUTOMATIONSERVICE} ${DOMOSERVICECHANNEL}
	Domoticz Hash......: ${HOMEAUTOMATIONHASH}	
	Domoticz Uptime....: Domoticz is ${DOMOSERVICE} since ${DOMOSERVICEUPTIME}
	
	$(tput setaf 2)SYSTEM DATA$(tput sgr0)
	OS Release.........: ${RELEASE}
	Kernel.............: ${KERNEL}
	System Uptime......: Up for ${UPTIME}
	Load Averages......: ${one}, ${five}, ${fifteen} (1, 5, 15 min)
	Processes..........: ${PSA} processes running of which ${PSU} are yours
	CPU Temp...........: ${CPUTEMP} @ $(($CPUMHZ/1000)) Mhz
	
	$(tput setaf 2)STORAGE / MEMORY DATA$(tput sgr0)
			     Total:	 Used:      Free:
	Root...............:  ${HDD1}
	Storage............:  ${HDD2}	
	Memory.............:  ${MEM}
	Swap...............:  ${SWAP}
	
	$(tput setaf 2)CONNECTIVITY$(tput sgr0)
	WLAN IP............: ${WLANIP}	
	LAN IP.............: ${LANIP}	| Bluetooth......: Bluetooth is ${BTSERVICE}
	WAN IP.............: ${WANIP}	| WLAN...........: WLAN is ${WLANSERVICE}
	
	$(tput setaf 2)MAINTENANCE$(tput sgr0)
	Repo Updates.......: $(tput setaf 1)${REPOUPDATESAVAIL} $(tput sgr0)Repository updates available
	Rpi Updates........: $(tput setaf 1)${RPIUPDATESAVAIL} $(tput sgr0)Rpi update available
	Domoticz Updates...: $(tput setaf 1)${DOMOUPDATESAVAIL} $(tput sgr0)Domoticz update available ${DOMOUPDATEFROM} ${DOMOUPDATETO}
 $(tput sgr0) "