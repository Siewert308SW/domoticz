--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_activity_toilet.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-4-2017
	@Script to switch ON toilet light when motion is triggered with standby to avoid triggering it again when light set OFF
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- IsDark Switches and Trigger									-- Light Switches
	local toilet_motion_detector		= 'W.C Motion'				local toilet_light					= 'W.C Lamp'

-- Various Switches
	local toilet_standby				= 'W.C Standby'
	local upstairs_standby				= 'Trap Boven - Standby'
	local downstairs_standby			= 'Trap Beneden - Standby'
	local isdark_standby				= 'IsDonker - Standby'

--
-- **********************************************************
-- Toilet light ON at motion detection
-- **********************************************************
--

	if devicechanged[toilet_motion_detector] == 'On' 
		and otherdevices[toilet_light]  == 'Off'
		and otherdevices[toilet_standby]  == 'Off'
		and otherdevices[isdark_standby]  == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Brown">-- '..toilet_light..' ==> '..toilet_motion_detector..' has motion detected, '..toilet_light..' switched ON...</font>')	
		end		
		commandArray[toilet_standby]='On'		
		commandArray[toilet_light]='On'	
	end

--
-- **********************************************************
-- Toilet light manual ON
-- **********************************************************
--

	if devicechanged[toilet_light] == 'On'
		and otherdevices[toilet_motion_detector]  == 'Off'
		and otherdevices[toilet_standby]  == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Brown">-- '..toilet_light..' ==> '..toilet_light..' switched ON manually...</font>')	
		end		
		commandArray[toilet_standby]='On'
	end

--
-- **********************************************************
-- Toilet light manual OFF
-- **********************************************************
--

	if devicechanged[toilet_light] == 'Off'
		and otherdevices[toilet_standby]  == 'On'		
	then
		if verbose == 1 then	
		print('<font color="Brown">-- '..toilet_light..' ==> '..toilet_light..' switched OFF manually...</font>')	
		end			
		commandArray[toilet_standby]='Off AFTER 10'
	end