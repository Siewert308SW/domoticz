--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_activity_doorbell.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-2-2017
	@Script to increase frontdoor light lumen and switch hallway light ON/OFF when someone rang the doorbell
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Doorbell locals														-- Various locals
	local doorbell 							= 'Deurbel'						local someonehome						= 'Iemand Thuis'
	local doorbell_trigger 					= 'Deurbel_2'					local motion_upstairs 					= 'Trap Motion Boven'
	local doorbell_standby 					= 'Deurbel - Standby'			local upstairs_standby 					= 'Trap Boven - Standby'

-- Door Sensors															-- Variables
	local frontdoor							= 'Voor Deur'					local isdoorbellstandby					= 'IsDoorbell_Standby'
	local livingroom_door					= 'Kamer Deur'					local isshower_variable					= 'IsDouchen'

-- Lights																-- IsDark Switches
	local hallway_light						= 'Gang Wandlamp'				local isdark_front_door_trigger 		= 'IsDonker_Tuin_Verlichting'
	local front_door_light					= 'Tuin Voordeur Verlichting'
	local warn_light						= 'Deurbel - Blink'
	local min_lum							= 7
	local max_lum							= 70

--
-- **********************************************************
-- Doorbell pressed, Ring doorbell twice
-- **********************************************************
--

	if devicechanged[doorbell] == 'Group On'
		and otherdevices[doorbell_standby]   == 'Off'	
		and otherdevices[frontdoor]   == 'Closed'
		and otherdevices[front_door_light]   == 'Off'		
	then
		commandArray[doorbell_standby]='On'
		commandArray[doorbell_trigger]='Group On AFTER 4'		
		if verbose == 1 then	
		print('<font color="Darkgreen">-- '..doorbell..' ==> Someone activated '..doorbell..'...</font>')	
		end		
	end

	if devicechanged[doorbell] == 'Group On'
		and otherdevices[doorbell_standby]   == 'Off'	
		and otherdevices[frontdoor]   == 'Closed'
		and otherdevices[front_door_light]   ~= 'Off'		
	then
		commandArray[doorbell_standby]='On'
		commandArray[doorbell_trigger]='Group On AFTER 4'
		commandArray[front_door_light]='Set Level '..max_lum..' AFTER 6'		
		if verbose == 1 then	
		print('<font color="Darkgreen">-- '..doorbell..' ==> Someone activated '..doorbell..', '..front_door_light..' lumen set to Level '..max_lum..'...</font>')	
		end		
	end
	
	if devicechanged[doorbell_standby] == 'Off'	
		and otherdevices[isdark_front_door_trigger ]   == 'On'	
		and otherdevices[front_door_light]   ~= 'Off'
		and uservariables[isdoorbellstandby]   == 0		
	then
		commandArray[front_door_light]='Set Level '..min_lum..' AFTER 30'
		if verbose == 1 then	
		print('<font color="Darkgreen">-- '..doorbell..' ==> '..doorbell_standby..' deactivated, '..front_door_light..' lumen set to Level '..min_lum..'...</font>')	
		end		
	end		
	

--
-- **********************************************************
-- Doorbell Activity via Livingroom Door
-- **********************************************************
--

	if devicechanged[livingroom_door] == 'Open'
		and otherdevices[frontdoor]   == 'Closed'
		and otherdevices[someonehome]   == 'On'	
		and otherdevices[isdark_front_door_trigger]   == 'On'	
		and otherdevices[front_door_light]   ~= 'Off'	
		and otherdevices[doorbell_standby]   == 'On'	
		and otherdevices[hallway_light]   == 'Off'	
		and uservariables[isdoorbellstandby]   == 0
		and otherdevices[motion_upstairs]   == 'Off'
		and otherdevices[upstairs_standby]  == 'Off'		
	then
		commandArray["Variable:" .. isdoorbellstandby .. ""]= '1'	
		commandArray[hallway_light]='On AFTER 1'
		if verbose == 1 then	
		print('<font color="Darkgreen">-- '..doorbell..' ==> Someone responded to the '..doorbell..' via '..livingroom_door..', '..hallway_light..' switched ON...</font>')	
		end			
	end

--
-- **********************************************************
-- Doorbell Activity via Frontdoor
-- **********************************************************
--

	if devicechanged[frontdoor] == 'Open'
		and otherdevices[someonehome]   == 'On'	
		and otherdevices[isdark_front_door_trigger]   == 'On'	
		and otherdevices[front_door_light]   ~= 'Off'	
		and otherdevices[hallway_light]   == 'On'	
		and uservariables[isdoorbellstandby]   == 1
	then
		commandArray["Variable:" .. isdoorbellstandby .. ""]= '2'	
		commandArray[hallway_light]='On AFTER 1'
		commandArray[front_door_light]='Set Level '..max_lum..' AFTER 6'		
	end

	if devicechanged[frontdoor] == 'Closed'
		and otherdevices[someonehome]   == 'On'	
		and otherdevices[isdark_front_door_trigger]   == 'On'	
		and otherdevices[front_door_light]   ~= 'Off'	
		and otherdevices[hallway_light]   == 'On'	
		and uservariables[isdoorbellstandby]   == 2
	then
		commandArray["Variable:" .. isdoorbellstandby .. ""]= '3'		
	end

	if devicechanged[livingroom_door] == 'Closed'
		and otherdevices[frontdoor]   == 'Closed'	
		and otherdevices[someonehome]   == 'On'	
		and otherdevices[isdark_front_door_trigger]   == 'On'	
		and otherdevices[front_door_light]   ~= 'Off'	
		and otherdevices[hallway_light]   == 'On'	
		and uservariables[isdoorbellstandby]   == 3
	then
		commandArray["Variable:" .. isdoorbellstandby .. ""]= '0'	
		commandArray[hallway_light]='Off AFTER 15'
		commandArray[front_door_light]='Set Level 7 AFTER 60'
		if verbose == 1 then	
		print('<font color="Darkgreen">-- '..doorbell..' ==> Assuming everybody is in and settled, '..hallway_light..' switching OFF in 15 seconds...</font>')	
		end		
	end