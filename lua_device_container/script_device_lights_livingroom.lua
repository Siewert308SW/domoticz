--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_lights_livingroom.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-5-2017
	@Script to switch various livingroom light scenes ON/OFF
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- IsDark Switches													-- Light Switches
	local isdark_living_room_trigger1	= 'IsDonker_Woonkamer_1'		local livingroom_standing_light		= 'Woonkamer Sta Lamp'
	local isdark_living_room_trigger2	= 'IsDonker_Woonkamer_2'		local livingroom_wall_lights		= 'Woonkamer Wandlampen'
	local isdark_standby				= 'IsDonker - Standby'			local twilight						= 'Woonkamer Schemerlamp'
	local isdark_sunset					= 'Sunrise/Sunset'				local livingroom_light_switch		= 'Woonkamer Verlichting Knop'

-- Scenes															-- Variables
	local stage_1						= 'Woonkamer_Stage_1'			local var_daytime					= 'Var_IsTimeOfTheDay'
	local stage_2						= 'Woonkamer_Stage_2'			local iskerst						= 'Feestdagen'
	local stage_3						= 'Woonkamer_Stage_3'			local livingroom_lights_timeout		= 5
	local stage_4						= 'Romantic'					local standby_lights_timeout		= 60				
	local stage_1_xmas					= 'Woonkamer_Kerst_1'			local isshower_variable				= 'IsDouchen'
	local stage_2_xmas					= 'Woonkamer_Kerst_2'

-- Various Switches
	local someonehome					= 'Iemand Thuis'
	local someonehome_standby			= 'Iemand Thuis - Standby'

--
-- **********************************************************
-- Livingroom lights ON/OFF when SomeOneHome and IsDark and IsNotXmas
-- **********************************************************
--

	if devicechanged[someonehome]   == 'On'
		and otherdevices[isdark_living_room_trigger1]   == 'On'
		and otherdevices[isdark_living_room_trigger2]   == 'Off'	
		and otherdevices[livingroom_standing_light]  == 'Off'
		and otherdevices[isdark_standby]   == 'Off'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..someonehome..' activated, Scene:'..stage_1..' will be switched ON in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_1.. ""]='On'
	end

------------

	if devicechanged[someonehome]   == 'On'
		and otherdevices[isdark_living_room_trigger1]   == 'On'
		and otherdevices[isdark_living_room_trigger2]   == 'On'	
		and otherdevices[livingroom_standing_light]  == 'Off'				
		and otherdevices[isdark_sunset]   == 'On' 
		and otherdevices[isdark_standby]   == 'Off'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..someonehome..' activated, Scene:'..stage_2..' will be switched ON in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_2.. ""]='On'
	end

------------

	if devicechanged[someonehome]   == 'On'
		and otherdevices[isdark_living_room_trigger1]   == 'On'
		and otherdevices[isdark_living_room_trigger2]   == 'On'	
		and otherdevices[livingroom_standing_light]  == 'Off'				
		and otherdevices[isdark_sunset]   == 'On' 
		and otherdevices[isdark_standby]   == 'On'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..someonehome..' activated, Scene:'..stage_3..' will be switched ON in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_3.. ""]='On'
	end
	
--
-- **********************************************************
-- Livingroom lights ON/OFF when IsDark and SomeOneHome and IsNotXmas
-- **********************************************************
--

	if devicechanged[isdark_living_room_trigger1]   == 'On'
		and otherdevices[isdark_living_room_trigger2]   == 'Off'		
		and otherdevices[someonehome]   == 'On'
		and otherdevices[livingroom_standing_light]  == 'Off'
		and otherdevices[isdark_standby]   == 'Off'
		and otherdevices[iskerst] == 'Off'
		and uservariables[isshower_variable] == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_living_room_trigger1..' activated, Scene:'..stage_1..' will be switched ON in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_1.. ""]='On AFTER '..livingroom_lights_timeout..''
	end
	
------------

	if devicechanged[isdark_living_room_trigger2]   == 'On'
		and otherdevices[isdark_living_room_trigger1]   == 'On'
		and otherdevices[someonehome]   == 'On'		
		and otherdevices[twilight]  == 'Off'				
		and otherdevices[isdark_sunset]   == 'On' 
		and otherdevices[isdark_standby]   == 'Off'
		and otherdevices[iskerst] == 'Off'
		and uservariables[isshower_variable] == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_living_room_trigger2..' activated, Scene:'..stage_2..' will be switched ON in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_2.. ""]='On AFTER '..livingroom_lights_timeout..''
	end

------------

	if devicechanged[isdark_sunset]   == 'On'
		and otherdevices[isdark_living_room_trigger1]   == 'On'
		and otherdevices[isdark_living_room_trigger2]   == 'On' 	
		and otherdevices[someonehome]   == 'On'		
		and otherdevices[twilight]  == 'Off'				
		and otherdevices[isdark_standby]   == 'Off'
		and otherdevices[iskerst] == 'Off'
		and uservariables[isshower_variable] == 0		
	then	
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_sunset..' activated, Scene:'..stage_2..' will be switched ON...</font>')	
		end		
		commandArray["Scene:" ..stage_2.. ""]='On'
	end
	
--
-- **********************************************************
-- Livingroom lights OFF when NOT IsDark and IsNotXmas
-- **********************************************************
--

	if devicechanged[isdark_living_room_trigger1] == 'Off' 
		and otherdevices[livingroom_standing_light]  ~= 'Off'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_living_room_trigger1..' deactivated, Scene:'..stage_2..' will be switched OFF in '..livingroom_lights_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..stage_2.. ""]='Off AFTER '..livingroom_lights_timeout..''
	end

--
-- **********************************************************
-- Livingroom lights OFF when SomeOneHome is OFF  and IsNotXmas (also activated when toggling light switch)
-- **********************************************************
--

	if devicechanged[someonehome] == 'Off'	
		and otherdevices[someonehome_standby]  == 'Off'	
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..someonehome..' dectivated, Scene:'..stage_2..' will be switched OFF...</font>')	
		end		
		commandArray["Scene:" ..stage_2.. ""]='Off'
	end
	
--
-- **********************************************************
-- Some one home ON/OFF when a light switch is toggled
-- **********************************************************
--

	if devicechanged[livingroom_light_switch] == 'On' and otherdevices [someonehome] == 'Off' and otherdevices[iskerst] == 'Off'	
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..livingroom_light_switch..' ==> '..livingroom_light_switch..' toggled ON...</font>')	
		end	
		commandArray[someonehome]='On'			
	end	

	if devicechanged[livingroom_light_switch] == 'On' and otherdevices [someonehome] == 'On' and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..livingroom_light_switch..' ==> '..livingroom_light_switch..' toggled ON...</font>')	
		end	
		commandArray["Scene:" ..stage_2.. ""]='On'				
	end		
-------------------------------------------------------------------------------	
	if devicechanged[livingroom_light_switch] == 'Off' and otherdevices [someonehome] == 'On' and otherdevices[iskerst] == 'Off'	
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..livingroom_light_switch..' ==> '..livingroom_light_switch..' toggled OFF...</font>')	
		end	
		commandArray[someonehome]='Off'	
	end	
	
	if devicechanged[livingroom_light_switch] == 'Off' and otherdevices [someonehome] == 'Off' and otherdevices[iskerst] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..livingroom_light_switch..' ==> '..livingroom_light_switch..' toggled OFF...</font>')	
		end
		commandArray["Scene:" ..stage_2.. ""]='Off'		
	end		