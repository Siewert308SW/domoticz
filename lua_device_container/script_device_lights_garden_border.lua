--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_lights_garden_border.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-15-2017
	@Script to switch garden border lights ON/OFF when IsDark taking in count IsWeekend or IsNotWeekend
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- IsDark Switches															-- Various Switches
	local isdark_front_border_trigger 	= 'IsDonker_Border_Verlichting'			local someonehome					= 'Iemand Thuis'
	local isdark_sunset					= 'Sunrise/Sunset'						local someonehome_standby			= 'Iemand Thuis - Standby'
	local isdark_standby				= 'IsDonker - Standby'					local nobody_home					= 'Niemand Thuis'

-- Variables																-- Light Switches
	local isdark_front_border_variable	= 'IsDonker_Border_Verlichting_Standby'	local front_border_lights			= 'Tuin Border Verlichting'
	local isweekend_variable			= 'Var_IsWeekend'						local xmas_garden_lights			= 'Stopcontact - Voordeur'
	local iskerst						= 'Feestdagen'							
	local border_lights_verify			= 'Border_Verlichting_Verify'
	local border_scene_lights_timeout	= 60
	local pico_power_reset				= 'IsPIco_Power_Reset'
	
--
-- **********************************************************
-- Border Lights ON when dark and IsNotXmas
-- **********************************************************
--

	if devicechanged[isdark_front_border_trigger] == 'On'
		and otherdevices[isdark_sunset] == 'On'
		and otherdevices[front_border_lights] == 'Off'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> '..isdark_front_border_trigger..' activated, '..front_border_lights..' switched ON...</font>')	
		end
		commandArray[front_border_lights]='Set Level 50 AFTER 10'
	end

--
-- **********************************************************
-- Border Lights ON when dark standby ON and IsNotXmas
-- **********************************************************
--

	if devicechanged[isdark_sunset] == 'On'
		and otherdevices[isdark_front_border_trigger] == 'On'
		and otherdevices[front_border_lights] == 'Off'
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> '..isdark_sunset..' activated, '..front_border_lights..' switched ON...</font>')	
		end
		commandArray[front_border_lights]='Set Level 50 AFTER 20'
	end
	
--
-- **********************************************************
-- Border Lights Standby ON when some one home and IsNotXmas
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 15)
		and uservariables[isdark_front_border_variable] == 0	
		and otherdevices[someonehome] =='On'
		and otherdevices[iskerst] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> 22:15hr standby schedule: Set '..isdark_front_border_variable..'...</font>')	
		end					
		commandArray["Variable:" .. isdark_front_border_variable .. ""]= '1'
	end

--
-- **********************************************************
-- Border Lights OFF when some one at home and IsNotXmas
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 50)
		and otherdevices[isdark_front_border_trigger] == 'On'
		and otherdevices[front_border_lights] ~= 'Off'
		and uservariables[isdark_front_border_variable] == 1
		and uservariables[isweekend_variable] == 0
		and otherdevices[iskerst] == 'Off'
		and uservariables[border_lights_verify]   == 0		
		and otherdevices[someonehome] =='On'
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> 22:50hr weekday schedule: switched OFF '..front_border_lights..'...</font>')	
		end
		commandArray[front_border_lights]='Off AFTER 120'
		commandArray["Variable:" .. border_lights_verify .. ""]= '1'	
	end
	
--
-- **********************************************************
-- Border Lights OFF when nobody is home and IsNotXmas and IsNotWeekend
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 50)
		and otherdevices[isdark_front_border_trigger] == 'On'
		and otherdevices[front_border_lights] ~= 'Off'
		and uservariables[isdark_front_border_variable] == 0
		and otherdevices[someonehome] =='Off'
		and otherdevices[iskerst] == 'Off'
		and uservariables[border_lights_verify]   == 0
		and uservariables[isweekend_variable] == 0		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> 22:50hr weekday schedule: switched OFF '..front_border_lights..'...</font>')	
		end			
		commandArray[front_border_lights]='Off AFTER 120'
		commandArray["Variable:" .. border_lights_verify .. ""]= '1'			
	end	

--
-- **********************************************************
-- Border Lights OFF when nobody is home and IsNotXmas and IsWeekend
-- **********************************************************
--

	if (time.hour == 23) and (time.min == 30)
		and otherdevices[isdark_front_border_trigger] == 'On'
		and otherdevices[front_border_lights] ~= 'Off'
		and uservariables[isdark_front_border_variable] == 0
		and otherdevices[someonehome] =='Off'
		and otherdevices[iskerst] == 'Off'
		and uservariables[border_lights_verify]   == 0
		and uservariables[isweekend_variable] == 1		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> 23:30hr weekend schedule: switched OFF '..front_border_lights..'...</font>')	
		end			
		commandArray[front_border_lights]='Off AFTER 120'
		commandArray["Variable:" .. border_lights_verify .. ""]= '1'			
	end

--
-- **********************************************************
-- Border Lights OFF when IsDark OFF and IsNotXmas
-- **********************************************************
--

	if devicechanged[isdark_front_border_trigger] == 'Off'
		and otherdevices[iskerst] == 'Off'	
	then	
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> '..isdark_front_border_trigger..' deactivated, '..front_border_lights..' switched OFF...</font>')	
		end			
		commandArray[front_border_lights]='Off AFTER 80'
		commandArray["Variable:" .. isdark_front_border_variable .. ""]= '0'
		commandArray["Variable:" .. border_lights_verify .. ""]= '0'		
	end
	
--
-- **********************************************************
-- Border lights OFF when Nobody at home and IsNotXmas
-- **********************************************************
--

	if devicechanged[someonehome_standby] == 'Off'	
		and otherdevices[someonehome]  == 'Off'
		and otherdevices[front_border_lights] ~= 'Off'
		and otherdevices[isdark_front_border_trigger] == 'On'
		and uservariables[isdark_front_border_variable] == 1
		and otherdevices[iskerst] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..front_border_lights..' ==> '..someonehome_standby..' deactivated, Scene:'..front_border_lights..' will be switched OFF in '..border_scene_lights_timeout..' seconds...</font>')	
		end	
		commandArray[front_border_lights]='Off AFTER 80'
	end
	
--
-- **********************************************************
-- Restore Border lights state when power outage is over
-- **********************************************************
--	

	if devicechanged[someonehome] == 'On'
		and otherdevices[isdark_sunset] == 'On'	
		and otherdevices[isdark_front_border_trigger] == 'On'
		and otherdevices[iskerst] == 'Off'	
		and uservariables[isdark_front_border_variable] == 0		
		and uservariables[pico_power_reset] == 1		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..front_border_lights..' ==> '..pico_power..' activated, '..front_border_lights..' switched ON...</font>')	
		end
		commandArray[front_border_lights]='Set Level 50 AFTER 60'
	end