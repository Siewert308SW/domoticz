--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_switch_laptop.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Script for switching dummy laptop switch to determine if SomeOneHome
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Laptops
	local laptop_1 						= 'Jerina Laptop'
	local laptop_2 						= 'Siewert Laptop'
	local laptop_3 						= 'Natalya Laptop'
	local laptop_switch 				= 'Laptops'
	local laptop_2_killer 				= 'Standby Killer (Siewert Laptop)'
	local laptop_2_killer_timeout 		= 30
	
--
-- **********************************************************
-- Laptop 1 ON/OFF
-- **********************************************************
--
	if devicechanged[laptop_1] == 'On' 
		and otherdevices[laptop_switch] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_1..' online, '..laptop_switch..' switch activated...</font>')	
		end
		commandArray[laptop_switch]='On'	
	end
	
	if devicechanged[laptop_1] == 'Off'
		and otherdevices[laptop_2] == 'Off'
		and otherdevices[laptop_3] == 'Off' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_1..' offline, '..laptop_switch..' switch deactivated...</font>')	
		end	
		commandArray[laptop_switch]='Off'	
	end
	
--
-- **********************************************************
-- Laptop 3 ON/OFF
-- **********************************************************
--
	if devicechanged[laptop_3] == 'On' 
		and otherdevices[laptop_switch] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_3..' online, '..laptop_switch..' switch activated...</font>')	
		end	
		commandArray[laptop_switch]='On'	
	end
	
	if devicechanged[laptop_3] == 'Off'
		and otherdevices[laptop_2] == 'Off'
		and otherdevices[laptop_1] == 'Off' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_3..' offline, '..laptop_switch..' switch deactivated...</font>')	
		end	
		commandArray[laptop_switch]='Off'	
	end	

--
-- **********************************************************
-- Laptop 2 ON/OFF including StandbyKiller
-- **********************************************************
--
	if devicechanged[laptop_2] == 'On'	
		and otherdevices[laptop_switch] == 'Off'
	then
		if verbose == 1 then
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' online, '..laptop_switch..' switch activated and '..laptop_2_killer..' will be switched ON in '..laptop_2_killer_timeout..' seconds...</font>')			
		end	
		commandArray[laptop_switch]='On'
		commandArray[laptop_2_killer]='On AFTER '..laptop_2_killer_timeout..''
	end

	if devicechanged[laptop_2] == 'On'
		and otherdevices[laptop_3] == 'On'
		and otherdevices[laptop_1] == 'Off' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' online, '..laptop_2_killer..' will be switched ON in '..laptop_2_killer_timeout..' seconds...</font>')	
		end		
		commandArray[laptop_2_killer]='On AFTER '..laptop_2_killer_timeout..''
	end
	
	if devicechanged[laptop_2] == 'On'
		and otherdevices[laptop_3] == 'Off'
		and otherdevices[laptop_1] == 'On' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' online, '..laptop_2_killer..' will be switched ON in '..laptop_2_killer_timeout..' seconds...</font>')	
		end	
		commandArray[laptop_2_killer]='On AFTER '..laptop_2_killer_timeout..''
	end		

	if devicechanged[laptop_2] == 'On'
		and otherdevices[laptop_3] == 'On'
		and otherdevices[laptop_1] == 'On' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' online, '..laptop_2_killer..' will be switched ON in '..laptop_2_killer_timeout..' seconds...</font>')	
		end	
		commandArray[laptop_2_killer]='On AFTER '..laptop_2_killer_timeout..''
	end		
	
	if devicechanged[laptop_2] == 'Off'
		and otherdevices[laptop_3] == 'Off'
		and otherdevices[laptop_1] == 'Off' 
		and otherdevices[laptop_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' offline, '..laptop_2_killer..' will be switched OFF in '..laptop_2_killer_timeout..' seconds and '..laptop_switch..' switch deactivated...</font>')	
		end	
		commandArray[laptop_switch]='Off'
		commandArray[laptop_2_killer]='Off AFTER '..laptop_2_killer_timeout..''	
	
	end
	
	if devicechanged[laptop_2] == 'Off'
		and otherdevices[laptop_switch] == 'On'	
		and otherdevices[laptop_3] == 'On'
		and otherdevices[laptop_1] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' offline, '..laptop_2_killer..' will be switched OFF in '..laptop_2_killer_timeout..' seconds...</font>')	
		end		
		commandArray[laptop_2_killer]='Off AFTER '..laptop_2_killer_timeout..''
	end
	
	if devicechanged[laptop_2] == 'Off'
		and otherdevices[laptop_switch] == 'On'	
		and otherdevices[laptop_3] == 'Off'
		and otherdevices[laptop_1] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' offline, '..laptop_2_killer..' will be switched OFF in '..laptop_2_killer_timeout..' seconds...</font>')	
		end		
		commandArray[laptop_2_killer]='Off AFTER '..laptop_2_killer_timeout..''
	end	
	
	if devicechanged[laptop_2] == 'Off'
		and otherdevices[laptop_switch] == 'On'	
		and otherdevices[laptop_3] == 'On'
		and otherdevices[laptop_1] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..laptop_switch..' ==> '..laptop_2..' offline, '..laptop_2_killer..' will be switched OFF in '..laptop_2_killer_timeout..' seconds...</font>')	
		end		
		commandArray[laptop_2_killer]='Off AFTER '..laptop_2_killer_timeout..''
	end	