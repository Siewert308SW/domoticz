--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_switch_media.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Script for switching media dummy switches to determine if SomeOneHome
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = All
local verbose = 1	

-- Media
	local television 					= 'Televisie'
	local mediabox 						= 'MediaBox'
	local media_switch 					= 'Media'
--
-- **********************************************************
-- Television ON/OFF
-- **********************************************************
--
	if devicechanged[television] == 'On'
		and otherdevices[mediabox] == 'On'
		and otherdevices[media_switch] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..media_switch..' ==> '..television..' & '..mediabox..' are online, '..media_switch..' switch activated...</font>')	
		end		
		commandArray[media_switch]='On'
	end

	if devicechanged[television] == 'Off'
		and otherdevices[mediabox] == 'Off'
		and otherdevices[media_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..media_switch..' ==> '..television..' & '..mediabox..' are offline, '..media_switch..' switch deactivated...</font>')	
		end		
		commandArray[media_switch]='Off'
	end

--
-- **********************************************************
-- MediaBox ON/OFF
-- **********************************************************
--
	if devicechanged[mediabox] == 'On'
		and otherdevices[television] == 'On'
		and otherdevices[media_switch] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..media_switch..' ==> '..mediabox..' & '..television..' are online, '..media_switch..' switch activated...</font>')	
		end	
		commandArray[media_switch]='On'
	end

	if devicechanged[mediabox] == 'Off'
		and otherdevices[television] == 'Off'	
		and otherdevices[media_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..media_switch..' ==> '..mediabox..' & '..television..' are offline, '..media_switch..' switch deactivated...</font>')	
		end	
		commandArray[media_switch]='Off'
	end