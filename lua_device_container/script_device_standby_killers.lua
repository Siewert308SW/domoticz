--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_standby_killers.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-16-2017
	@Script to switch StandbyKillers ON/OFF when nobody at home after x minutes (see script_time_nobody_home.lua)
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

--
-- **********************************************************
-- Nobody Home Settings
-- **********************************************************
--	

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Standby Killers													-- Various Switches
	local boiler					= 'Standby Killer (Water Koker)'	local media_box 				= 'MediaBox'
	local tv_corner_killer 			= 'Standby Killer (T.V Hoek)'

-- Some / No body Home Switches										-- Variables
	local someonehome				= 'Iemand Thuis'					local killer_scene_timeout_on	= 10
	local someonehome_standby		= 'Iemand Thuis - Standby'			local killer_scene_timeout_on2	= 20
	local nobody_home				= 'Niemand Thuis'					local killer_scene_timeout_off	= 120
																		local killer_scene_timeout_off2	= 130
																		local pico_power_reset			= 'IsPIco_Power_Reset'
-- Scenes																
	local killer_scene_on			= 'Standby Killers ON'
	local killer_scene_off			= 'Standby Killers OFF'

--
-- **********************************************************
-- Standby Killers - Triggers
-- **********************************************************
--

	if devicechanged[nobody_home] == 'On' and uservariables[pico_power_reset] == 0
		and otherdevices[media_box]   == 'Off'	
		and otherdevices[boiler]   == 'On'		
	then
		if verbose == 1 then	
		print('<font color="Green">-- '..nobody_home..' ==> '..nobody_home..' activated, Scene:'..killer_scene_off..' will be switched OFF in '..killer_scene_timeout_off..' seconds...</font>')		
		end
		commandArray["Scene:" ..killer_scene_off.. ""]='Off AFTER '..killer_scene_timeout_off..''
		commandArray["Scene:" ..killer_scene_on.. ""]='Off AFTER 90'	
	end
	
--
-- **********************************************************
-- Standby Killers - Triggers when having a power outage
-- **********************************************************
--

	if devicechanged[nobody_home] == 'On' and uservariables[pico_power_reset] == 1
		and otherdevices[boiler]   == 'On'		
	then
		if verbose == 1 then	
		print('<font color="Green">-- '..nobody_home..' ==> '..nobody_home..' activated, Scene:'..killer_scene_off..' will be switched OFF NOW...</font>')		
		end
		commandArray["Scene:" ..killer_scene_on.. ""]='Off'	
	end	

---------

	if devicechanged[media_box] == 'Off'
		and otherdevices[someonehome]   == 'Off'			
		and otherdevices[tv_corner_killer]   == 'On'
	then
		if verbose == 1 then	
		print('<font color="Green">-- '..nobody_home..' ==> '..media_box..' deactivated, '..killer_scene_off..' will be switched OFF in '..killer_scene_timeout_off..' seconds...</font>')	
		end		
		commandArray["Scene:" ..killer_scene_off.. ""]='Off AFTER '..killer_scene_timeout_off..''
		commandArray["Scene:" ..killer_scene_on.. ""]='Off AFTER 90'
	end	
	
---------
	if devicechanged[someonehome]   == 'On'	
		and uservariables[pico_power_reset] == 0	
		and otherdevices[boiler]   == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated, '..killer_scene_on..' will be switched ON in '..killer_scene_timeout_on..' seconds...</font>')	
		end	
		commandArray["Scene:" ..killer_scene_on.. ""]='On AFTER '..killer_scene_timeout_on..''
	end	
	
--
-- **********************************************************
-- Reset power outage lock
-- **********************************************************
--

	if devicechanged[someonehome]   == 'On'	
		and uservariables[pico_power_reset] == 1	
		and otherdevices[boiler]   == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated, '..killer_scene_on..' will be switched ON in '..killer_scene_timeout_on..' seconds...</font>')	
		end	
		commandArray["Scene:" ..killer_scene_on.. ""]='On AFTER '..killer_scene_timeout_on..''
		commandArray['Variable:' .. pico_power_reset] = '0'	
		
	end	