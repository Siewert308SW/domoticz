--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_lights_dinnertable.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-19-2017
	@Script to switch diner table light ON/OFF with taking in count Laptops ON/OFF 
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]] 

-- Logging: 0 = None, 1 = All
local verbose = 1

local laptop_switch 				= 'Laptops'
local isdark_dinner_table 			= 'IsDonker_Eettafel'
local dinner_table_light 			= 'Woonkamer Eettafel Lamp'
local dinnertable_light_switch		= 'Woonkamer Eettafel Verlichting Knop'
local someonehome					= 'Iemand Thuis'
local someonehome_standby			= 'Iemand Thuis - Standby'

-- Motion Detector
local dinner_table_motion 			= 'Motion Eettafel'

-- Variables
local dinner_table_light_level		= 7
local dinner_table_light_timeout	= 5
local isshower_variable				= 'IsDouchen'
local dinnertable_light_standby		= 'IsDinnerTable_Light_Standby'
local dinnertable_light_standby_counter 		= 'IsDinnerTable_Light_Standby_Counter'

--
-- **********************************************************
-- Dinner table light ON/OFF when a laptop is online
-- **********************************************************
--

	if devicechanged[laptop_switch] == 'On'
		and otherdevices[isdark_dinner_table] == 'On'
		and otherdevices[dinner_table_light] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..laptop_switch..' activated, '..dinner_table_light..' switched ON in '..dinner_table_light_timeout..' seconds...</font>')	
		end		
		commandArray[dinner_table_light]='Set Level '..dinner_table_light_level..' AFTER '..dinner_table_light_timeout..''
	end

----------

	if devicechanged[laptop_switch] == 'Off'
		and otherdevices[dinner_table_light] ~= 'Off'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..laptop_switch..' deactivated, '..dinner_table_light..' switched OFF in '..dinner_table_light_timeout..' seconds...</font>')	
		end		
	commandArray[dinner_table_light]='Off AFTER '..dinner_table_light_timeout..''
	end

----------

--
-- **********************************************************
-- Dinner table light ON/OFF when IsDark and a laptop is online
-- **********************************************************
--

	if devicechanged[isdark_dinner_table] == 'On'
		and otherdevices[laptop_switch] == 'On'
		and otherdevices[dinner_table_light] == 'Off'
		and uservariables[isshower_variable] == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..isdark_dinner_table..' activated, '..dinner_table_light..' switched ON in '..dinner_table_light_timeout..' seconds...</font>')	
		end		
	commandArray[dinner_table_light]='Set Level 7 AFTER '..dinner_table_light_timeout..''
	end

----------

	if devicechanged[isdark_dinner_table] == 'Off'
		and otherdevices[dinner_table_light] ~= 'Off'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..isdark_dinner_table..' deactivated, '..dinner_table_light..' switched OFF in '..dinner_table_light_timeout..' seconds...</font>')	
		end		
	commandArray[dinner_table_light]='Off AFTER '..dinner_table_light_timeout..''
	end

--
-- **********************************************************
-- Dinner table light switch activity
-- **********************************************************
--
	
	if devicechanged[dinnertable_light_switch] == 'On'	
		and otherdevices[someonehome] == 'Off'	
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinnertable_light_switch..' ==> '..dinnertable_light_switch..' toggled while '..someonehome..' is OFF, '..dinner_table_light..' switched ON & '..someonehome..' activated...</font>')	
		end		
		commandArray[someonehome]='On'
	end	
	
--
-- **********************************************************
-- Dinner table light ON when someone is showering (see:script_device_activity_shower.lua)
-- **********************************************************
--

	if devicechanged[someonehome] == 'On'
		and otherdevices[isdark_dinner_table] == 'On'	
		and otherdevices[laptop_switch] == 'On'
		and otherdevices[dinner_table_light] == 'Off'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..someonehome..' activated, '..dinner_table_light..' switched ON in '..dinner_table_light_timeout..' seconds...</font>')	
		end		
		commandArray[dinner_table_light]='Set Level 7 AFTER '..dinner_table_light_timeout..''
	end	
	
--
-- **********************************************************
-- Turn ON Dinner table light ON when motion detected and no Laptops ON
-- **********************************************************
--	

	if devicechanged[dinner_table_motion] == 'On'
		and otherdevices[isdark_dinner_table] == 'On'	
		and otherdevices[laptop_switch] == 'Off'
		and otherdevices[dinner_table_light] == 'Off'
		and uservariables[dinnertable_light_standby] == 0			
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..dinner_table_motion..' activated, '..dinner_table_light..' switched ON in '..dinner_table_light_timeout..' seconds...</font>')	
		end	
		commandArray['Variable:' .. dinnertable_light_standby] = '1'		
		commandArray[dinner_table_light]='Set Level 25 AFTER '..dinner_table_light_timeout..''
	end	

-- reset timer	
	if devicechanged[dinner_table_motion] == 'On'
		and uservariables[dinnertable_light_standby] == 1			
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..dinner_table_light..' ==> '..dinner_table_motion..' activated, '..dinner_table_light..' standby reset...</font>')	
		end	
		commandArray['Variable:' .. dinnertable_light_standby_counter] = '0'
	end		
