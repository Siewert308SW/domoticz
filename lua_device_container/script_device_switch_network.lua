--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_switch_network.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Script for switching network dummy switches to determine if SomeOneHome
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = All
local verbose = 1	

-- Network
	local internet 						= 'Ethernet'
	local router 						= 'Router'
	local network 						= 'Netwerk'
--
-- **********************************************************
-- Internet ON/OFF
-- **********************************************************
--
	if devicechanged[internet] == 'On'
		and otherdevices[router] == 'On'
		and otherdevices[network] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..network..' ==> '..internet..' & '..router..' are back online, '..network..' switch activated...</font>')	
		end			
		commandArray[network]='On'
	end

	if devicechanged[internet] == 'Off'
		and otherdevices[network] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..network..' ==> '..internet..' seems to be down, '..network..' switch deactivated...</font>')	
		end		
		commandArray[network]='Off'
	end

--
-- **********************************************************
-- Netwerk ON/OFF
-- **********************************************************
--
	if devicechanged[router] == 'On'
		and otherdevices[internet] == 'On'
		and otherdevices[network] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..network..' ==> '..router..' & '..internet..' are back online, '..network..' switch activated...</font>')	
		end		
		commandArray[network]='On'
	end

	if devicechanged[router] == 'Off'
		and otherdevices[network] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..network..' ==> '..router..' seem to be down, '..network..' switch deactivated...</font>')	
		end		
		commandArray[network]='Off'
	end