--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_switch_phones.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Script for switching phone dummy switch to determine if SomeOneHome
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Phones
	local phone_1 						= 'Jerina GSM'
	local phone_2 						= 'Siewert GSM'
	local phone_3 						= 'Natalya GSM'
	local phone_switch 					= 'Telefoons'

--
-- **********************************************************
-- Phone 1 ON/OFF
-- **********************************************************
--
	if devicechanged[phone_1] == 'On' 
		and otherdevices[phone_switch] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_1..' online, '..phone_switch..' switch activated...</font>')	
		end		
		commandArray[phone_switch]='On'	
	end
	
	if devicechanged[phone_1] == 'Off'
		and otherdevices[phone_2] == 'Off'
		and otherdevices[phone_3] == 'Off' 
		and otherdevices[phone_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_1..' offline, '..phone_switch..' switch deactivated...</font>')	
		end		
		commandArray[phone_switch]='Off'	
	end
	
--
-- **********************************************************
-- Phone 2 ON/OFF
-- **********************************************************
--
	if devicechanged[phone_2] == 'On' 
		and otherdevices[phone_switch] == 'Off'
	then
		if verbose == 1 then
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_2..' online, '..phone_switch..' switch activated...</font>')
		end	
		commandArray[phone_switch]='On'	
	end
		
	if devicechanged[phone_2] == 'Off'
		and otherdevices[phone_3] == 'Off'
		and otherdevices[phone_1] == 'Off' 
		and otherdevices[phone_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_2..' offline, '..phone_switch..' switch deactivated...</font>')	
		end
		commandArray[phone_switch]='Off'	
	end	
	
--
-- **********************************************************
-- Phone 3 ON/OFF
-- **********************************************************
--
	if devicechanged[phone_3] == 'On' 
		and otherdevices[phone_switch] == 'Off'
		and (time.hour > 8) and (time.hour < 22) 		
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_3..' online, '..phone_switch..' switch activated...</font>')	
		end		
		commandArray[phone_switch]='On'	
	end
		
	if devicechanged[phone_3] == 'Off'
		and otherdevices[phone_2] == 'Off'
		and otherdevices[phone_1] == 'Off' 
		and otherdevices[phone_switch] == 'On'
	then
		if verbose == 1 then	
		print('<font color="purple">-- '..phone_switch..' ==> '..phone_3..' offline, '..phone_switch..' switch deactivated...</font>')	
		end		
		commandArray[phone_switch]='Off'	
	end