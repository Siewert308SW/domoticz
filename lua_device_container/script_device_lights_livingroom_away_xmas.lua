--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_lights_livingroom_away_xmas.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-4-2017
	@Script to switch ON/OFF Away light scene when nobody at home taking in count IsWeekend or IsNotWeekend
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- IsDark Switches													-- Lights
	local isdark_living_room_trigger1		= 'IsDonker_Woonkamer_1'	local twilight_light					= 'Woonkamer Schemerlamp'
	local isdark_standby					= 'IsDonker - Standby'

-- Various Switches													-- Scenes
	local someonehome						= 'Iemand Thuis'			local scene_away						= 'Woonkamer_Away'
	local someonehome_standby				= 'Iemand Thuis - Standby'	local nobody_away_scene					= 'Nobody Home'
																		local nobody_away_xmas_scene			= 'Nobody Home_Kerst'

-- Variables
	local isweekend_variable				= 'Var_IsWeekend'
	local iskerst							= 'Feestdagen'
	local away_scene_timeout				= 60
	local away_scene_timeout_xmas			= 80	
	local away_lights_verify				= 'Away_Verlichting_Verify'

--
-- **********************************************************
-- Twilight Scene ON when dark, nobody home activated and IsXmas
-- **********************************************************
--

	if devicechanged[someonehome_standby]   == 'On'
		and otherdevices[someonehome]   == 'Off'	
		and otherdevices[isdark_living_room_trigger1]   == 'On'	
		and otherdevices[twilight_light]  == 'Off' 
		and otherdevices[iskerst] == 'On'		
		and (time.hour > 15) and (time.hour < 22) 
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..nobody_home..' activated, Scene:'..scene_away_xmas..' will be switched ON after '..away_scene_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..scene_away_xmas.. ""]='On AFTER '..away_scene_timeout..''	
	end			
---------------------------	
	
	if devicechanged[isdark_living_room_trigger1]   == 'On'
		and otherdevices[someonehome]   == 'Off'	
		and otherdevices[twilight_light]  == 'Off' 
		and otherdevices[iskerst] == 'On'			
		and (time.hour > 15) and (time.hour < 22) 
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_living_room_trigger1..' activated, Scene:'..scene_away_xmas..' will be switched ON after '..away_scene_timeout..' seconds...</font>')	
		end		
		commandArray["Scene:" ..scene_away_xmas.. ""]='On AFTER '..away_scene_timeout..''	
	end		

--
-- **********************************************************
-- Twilight Scene OFF when IsNotWeekend and IsXmas and IsXmas
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 45)
		and otherdevices[someonehome]   == 'Off'		
		and otherdevices[isdark_living_room_trigger1]   == 'On'	
		and otherdevices[twilight_light]  ~= 'Off'				
		and uservariables[isweekend_variable] == 0
		and otherdevices[iskerst] == 'On'
		and uservariables[away_lights_verify]   == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> 22:45hr - switched OFF Scene:'..scene_away_xmas..' as scheduled at weekdays...</font>')	
		end		
		commandArray["Scene:" ..scene_away_xmas.. ""]='Off AFTER '..away_scene_timeout..''
		commandArray["Scene:" ..nobody_away_xmas_scene.. ""]='Off AFTER '..away_scene_timeout_xmas..''		
		commandArray["Variable:" .. away_lights_verify .. ""]= '1'		
	end		
	
--
-- **********************************************************
-- Twilight Scene OFF when IsWeekend and IsXmas and IsXmas
-- **********************************************************
--
	
	if (time.hour == 23) and (time.min == 15)
		and otherdevices[someonehome]   == 'Off'		
		and otherdevices[isdark_living_room_trigger1]   == 'On'		
		and otherdevices[twilight_light]  ~= 'Off'				
		and uservariables[isweekend_variable] == 1
		and otherdevices[iskerst] == 'On'
		and uservariables[away_lights_verify]   == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> 23:01hr - switched OFF Scene:'..scene_away_xmas..' as scheduled at weekends...</font>')	
		end	
		commandArray["Scene:" ..scene_away_xmas.. ""]='Off AFTER '..away_scene_timeout..''
		commandArray["Scene:" ..nobody_away_xmas_scene.. ""]='Off AFTER '..away_scene_timeout_xmas..''		
		commandArray["Variable:" .. away_lights_verify .. ""]= '1'		
	end

	if devicechanged[isdark_living_room_trigger1] == 'Off'
		and uservariables[away_lights_verify]   == 1
		and otherdevices[iskerst] == 'On'		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- Livingroom Lights ==> '..isdark_living_room_trigger1..' deactivated, '..away_lights_verify..' variable has been reset...</font>')	
		end		
		commandArray["Variable:" .. away_lights_verify .. ""]= '0'
		commandArray["Scene:" ..nobody_away_xmas_scene.. ""]='Off AFTER '..away_scene_timeout_xmas..''		
	end		