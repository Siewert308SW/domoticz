--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_ups_pico_power_control.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-16-2017
	@Script for PIco UPS HV3.0A to control and to be able to do a Domoticz database safe shutdown
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = Littel, 2 = ALL
local verbose = 2

-- PIco Devices
	local pico_voltage 					= 'PIco BAT Voltage'
	local pico_power    				= 'PIco RPi Powered'
	local pico_bat_percentage 			= 'PIco BAT Percentage'
	
-- PIco/RPi Variables
	local pico_power_control_switch		= 'IsPIco_Power_Switch_State'
	local pico_power_reset				= 'IsPIco_Power_Reset'	
	local pico_power_ping_counter		= 'IsPIco_Power_Ping_Counter'
	local outage_counter				= 'IsPIco_Power_Outage_Counter'	
	local pico_power_battery_low    	= 35
	local pico_power_ping_max	    	= 3 -- 1x per minute
	
-- Various Switches	
	local rpi_shutdown					= 'PIco RPi Shutdown'

-- Various Ping Variables	
	local router						= '192.168.178.1'
	
-- Power Outage Triggers	
	local someonehome					= 'Iemand Thuis'
	local someonehome_standby			= 'Iemand Thuis - Standby'	
	local nobody_home					= 'Niemand Thuis'
	local phone_switch 					= 'Telefoons'	

--
-- **********************************************************
-- PIco Power control, check if the RPi is running on Battery/RPi Power
-- **********************************************************
--
		
if devicechanged[pico_voltage] then
		pico_bat_current = tonumber (otherdevices_svalues[pico_bat_percentage])
		pico_ping_counter = tonumber (uservariables[pico_power_ping_counter])
		pico_ping_counter_remaining = pico_power_ping_max - pico_ping_counter -1
		pico_outage_counter = tonumber (uservariables[outage_counter])
		pico_outage_counter = pico_outage_counter + 1		
		
		ping_router_success=os.execute('ping -q -c1 -W 1 '..router..'')		
		
		local power = io.popen("i2cget -y 1 0x69 0x00")
		local pico_current_power = power:read("*n")
		power:close()
		
		if otherdevices[pico_power] == 'On' and pico_current_power == 0x02 then	--0x02
			commandArray[pico_power]='Off' 
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Looks like your RPi is running on battery power...</font>')			
			end
		end

		if otherdevices[pico_power] == 'Off' and pico_current_power == 0x01 then --0x01
			commandArray[pico_power]='On'	
			if uservariables[pico_power_control_switch] ~= 0 then	
			commandArray['Variable:' .. pico_power_control_switch]='0'				
			end	
			
			if uservariables[pico_power_ping_counter] ~= 0 then	
			commandArray['Variable:' .. pico_power_ping_counter]='0'				
			end			
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Looks like your RPi is running on RPi power again...</font>')	
			end		
		end	
		
--
-- **********************************************************
-- PIco Power control, Double check if router is online, if so then false alarm
-- **********************************************************
--
if otherdevices[pico_power] == 'Off' and uservariables[pico_power_control_switch] == 0 and pico_ping_counter ~= pico_power_ping_max then
	
	if ping_router_success then	
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Router seems to be online, assuming you aint got a power outage and this is a false report...</font>')	
			end	
	else
			if verbose >= 1 then
			print('<font color="darkred">-- '..pico_power..' ==> Router seems to be offline, assuming you got a power outage...</font>')				
			print('<font color="darkred">-- Ping test '..pico_ping_counter..': Router is offline, will ping '..pico_ping_counter_remaining..' more times.</font>')	
			end	
			pico_ping_counter = pico_ping_counter + 1	
			commandArray['Variable:' .. pico_power_ping_counter] = tostring(pico_ping_counter)		
	end
end

if otherdevices[pico_power] == 'Off' and uservariables[pico_power_control_switch] == 0 and pico_ping_counter == pico_power_ping_max then

	if ping_router_success then	
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Router seems to be online, assuming this is a false report...</font>')	
			end	
	else
			if otherdevices[someonehome] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Final ping test failed: Router is offline, Now i do believe you have a real power outage...</font>')	
			print('<font color="darkred">-- Starting to shut down your house virtually so it will come back to live when you have power again!</font>')				
			end			
			-- As it seems there is a power outage, then virtual shutdown house, so lights/killer switch ON if power is back and Isdark
			commandArray[1]={['Variable:' .. pico_power_control_switch]='1'}
			commandArray[2]={['Variable:' .. pico_power_reset]='1'}	
			commandArray[3]={['Variable:' .. outage_counter] = tostring(pico_outage_counter)}
			commandArray[4]={['UpdateDevice'] = '145|0|' .. tostring(pico_outage_counter)}			
			commandArray[5]={[someonehome]='Off'}	
			commandArray[6]={[someonehome_standby]='Off AFTER 1'}
			commandArray[7]={[nobody_home]='On AFTER 2'}	
			else
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> Final ping test failed: Router is offline, Now i do believe you have a real power outage...</font>')	
			print('<font color="darkred">-- Starting to shut down your house virtually!</font>')				
			end			
			commandArray[1]={['Variable:' .. pico_power_control_switch]='1'}
			commandArray[2]={['Variable:' .. outage_counter] = tostring(pico_outage_counter)}
			commandArray[3]={['UpdateDevice'] = '146|0|' .. tostring(pico_outage_counter)}				
			end	
			
	end
end
		
--
-- **********************************************************
-- PIco Power control, When battery empty then do a database safe shutdown
-- **********************************************************
--

		if otherdevices[pico_power] == 'Off' 
		and uservariables[pico_power_control_switch] == 1		
		and pico_bat_current > pico_power_battery_low
		then
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> RPi running on battery, batter level @ '..pico_bat_current..'%...</font>')
			end			
		end	

		if otherdevices[pico_power] == 'Off' 
		and uservariables[pico_power_control_switch] == 1		
		and pico_bat_current < pico_power_battery_low
		then
			if verbose >= 1 then	
			print('<font color="darkred">-- '..pico_power..' ==> RPi still running on battery, batter level is below '..pico_bat_current..'%...</font>')
			print('<font color="darkred">-- Shutting down system, bye bye!</font>')	
			end	
				commandArray[1]={['Variable:' .. pico_power_control_switch]='2'}
				commandArray[2]={[rpi_shutdown]='On'}
		end			
		
		
end --last end	