--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_someone_home.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-16-2017
	@Script for switching SomeOneHome ON/OFF 
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Devices														-- Various Switches
	local laptop_switch 				= 'Laptops'					local someonehome					= 'Iemand Thuis'
	local television 					= 'Televisie'				local someonehome_standby			= 'Iemand Thuis - Standby'
	local phone_switch 					= 'Telefoons'				local arrival_standby				= 'Aankomst - Standby'
	local network						= 'Netwerk'					local nobody_home					= 'Niemand Thuis'
	local pico_power    				= 'PIco RPi Powered'		local nest_away						= 'Nest Away'
																	local livingroom_light_switch		= 'Woonkamer Verlichting Knop'
																	local pico_power_reset				= 'IsPIco_Power_Reset'
-- Door/Window Sensors	
	local frontdoor						= 'Voor Deur'
	local backdoor			 			= 'Achter Deur'
	local livingroom_door				= 'Kamer Deur'
	local sliding_door	 				= 'Schuifpui'
	local scullery_door 				= 'Bijkeuken Deur'
	local motion_upstairs 				= 'Trap Motion Boven'
	
--
-- **********************************************************
-- Some one home ON when devices are online
-- **********************************************************
--

	if devicechanged[laptop_switch] == 'On' and uservariables[pico_power_reset] == 0
		and otherdevices[someonehome] == 'Off' 
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..laptop_switch..' ==> '..laptop_switch..' activated, '..someonehome..' switched ON...</font>')	
		end	
		commandArray[someonehome]='On'
	end

------------
	
	if devicechanged[television] == 'On' and uservariables[pico_power_reset] == 0
		and (time.hour > 8) and (time.hour < 22) 
		and otherdevices[someonehome] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..media_switch..' ==> '..television..' switched ON, '..someonehome..' switched ON...</font>')	
		end		
		commandArray[someonehome]='On'
	end

------------

	if devicechanged[phone_switch] == 'On' and uservariables[pico_power_reset] == 0
		and (time.hour > 8) and (time.hour < 22)
		and otherdevices[someonehome] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..phone_switch..' ==> '..phone_switch..' activated, '..someonehome..' switched ON...</font>')	
		end		
		commandArray[someonehome]='On'
	end
	
--
-- **********************************************************
-- Some one home OFF when devices are offline
-- **********************************************************
--
	
	if devicechanged[phone_switch] == 'Off'
		and otherdevices[laptop_switch] == 'Off'
		and otherdevices[television] == 'Off'			
		and otherdevices[someonehome] == 'On' 
		and otherdevices[someonehome_standby] == 'Off'		
		and otherdevices[network] == 'On'	
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..phone_switch..' ==> '..phone_switch..' deactivated, '..someonehome..' switched OFF...</font>')	
		end		
		commandArray[someonehome]='Off'
		commandArray[someonehome_standby]='Off AFTER 60'		
	end
	
	if devicechanged[phone_switch] == 'Off'
		and otherdevices[laptop_switch] == 'Off'
		and otherdevices[television] == 'Off'			
		and otherdevices[someonehome] == 'Off' 
		and otherdevices[someonehome_standby] == 'On'
		and otherdevices[network] == 'On'	
	then
		if verbose == 1 then	
		print('<font color="Purple">-- '..phone_switch..' ==> '..phone_switch..' deactivated, '..someonehome_standby..' switched OFF...</font>')	
		end
		commandArray[someonehome_standby]='Off AFTER 20'		
	end	
	
--
--
-- **********************************************************
-- Some One Home with no devices
-- **********************************************************
--

	if devicechanged[livingroom_door] and otherdevices[someonehome_standby] == 'Off' and otherdevices[nobody_home] == 'On' and uservariables[pico_power_reset] == 0 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated by '..livingroom_door..'...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end

	if devicechanged[frontdoor] and otherdevices[someonehome_standby] == 'Off' and otherdevices[nobody_home] == 'On' and uservariables[pico_power_reset] == 0 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated by '..frontdoor..'...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end
	
	if devicechanged[backdoor] and otherdevices[someonehome_standby] == 'Off' and otherdevices[nobody_home] == 'On' and uservariables[pico_power_reset] == 0 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated by '..backdoor..'...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end
	
	if devicechanged[sliding_door] and otherdevices[someonehome_standby] == 'Off' and otherdevices[nobody_home] == 'On' and uservariables[pico_power_reset] == 0 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated by '..sliding_door..'...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end
	
	if devicechanged[scullery_door] and otherdevices[someonehome_standby] == 'Off' and otherdevices[nobody_home] == 'On' and uservariables[pico_power_reset] == 0 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated by '..scullery_door..'...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end	
	
	if devicechanged[pico_power] == 'On' and otherdevices[someonehome] == 'Off' and uservariables[pico_power_reset] == 1 then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome..' ==> '..someonehome..' activated after restoring from a power outage...</font>')	
			end			
			commandArray[someonehome]='On AFTER 5'	
			commandArray[arrival_standby]='On'	
	end	
	
--
-- **********************************************************
-- Some One Home Standby OFF
-- **********************************************************
--

	if devicechanged[motion_upstairs] and otherdevices[someonehome_standby] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome_standby..' ==> '..someonehome_standby..' deactivated by '..livingroom_door..'...</font>')	
			end			
			commandArray[someonehome_standby]='Off AFTER 60'	
	end

	if devicechanged[frontdoor] and otherdevices[someonehome_standby] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome_standby..' ==> '..someonehome_standby..' deactivated by '..frontdoor..'...</font>')	
			end			
			commandArray[someonehome_standby]='Off AFTER 60'	
	end
	
	if devicechanged[backdoor] and otherdevices[someonehome_standby] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome_standby..' ==> '..someonehome_standby..' deactivated by '..backdoor..'...</font>')	
			end			
			commandArray[someonehome_standby]='Off AFTER 60'	
	end
	
	if devicechanged[sliding_door] and otherdevices[someonehome_standby] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome_standby..' ==> '..someonehome_standby..' deactivated by '..sliding_door..'...</font>')	
			end			
			commandArray[someonehome_standby]='Off AFTER 60'	
	end
	
	if devicechanged[scullery_door] and otherdevices[someonehome_standby] == 'On' and otherdevices[nobody_home] == 'Off' then
			if verbose == 1 then	
			print('<font color="Purple">-- '..someonehome_standby..' ==> '..someonehome_standby..' deactivated by '..scullery_door..'...</font>')	
			end			
			commandArray[someonehome_standby]='Off AFTER 60'	
	end	