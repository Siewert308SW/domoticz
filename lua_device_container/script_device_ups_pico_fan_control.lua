--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_ups_pico_fan_control.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-16-2017
	@Script for PIco UPS HV3.0A to control the optional PIco Fan Kit
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]	 

-- Logging: 0 = None, 1 = Littel, 2 = ALL
local verbose = 2

-- PIco Devices
	local pico_voltage 					= 'PIco BAT Voltage'
	local pico_fan_speed 				= 'PIco FAN Speed'
	local fan_selector 					= 'PIco Fan Control'
	
-- PIco/RPi Variables
	local pico_fan_control_counter 		= 'IsPIco_Fan_Counter'
	local pico_fan_control_switch		= 'IsPIco_Fan_Switch_State'	
	local pico_fan_control_timeout 		= 10
	
-- RPi Devices
	local rpi_temp						= 'Raspberry Temperature'	
	
-- Various Switches	
	local rpi_shutdown						= 'PIco RPi Shutdown'


	
pico_fan_control_minutes = tonumber(uservariables[pico_fan_control_counter])
pico_fan_control_remaining = pico_fan_control_timeout - pico_fan_control_minutes - 1
		
if devicechanged[pico_voltage] then

--
-- **********************************************************
-- PIco Fan control, check if speed is really set during unlock timer
-- **********************************************************
--


		local handle = io.popen("i2cget -y 1 0x6b 0x12")
		local fan_current_speed = handle:read("*n")
		handle:close()	
		
		if otherdevices[fan_selector] == 'Off' and fan_current_speed ~= 0x00 and uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == 1 then
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)		
		commandArray[fan_selector]='Off'
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> Fan speed isnt set properly, Fan speed corrected to OFF...</font>')	
		end
		end

		if otherdevices[fan_selector] == 'Level 25' and fan_current_speed ~= 0x19 and uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == 1 then
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		commandArray[fan_selector]='Set Level 10'
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> Fan speed isnt set properly, Fan speed corrected to 25%...</font>')	
		end
		end	

		if otherdevices[fan_selector] == 'Level 50' and fan_current_speed ~= 0x32 and uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == 1 then
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		commandArray[fan_selector]='Set Level 20'
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> Fan speed isnt set properly, Fan speed corrected to 50%...</font>')	
		end
		end

		if otherdevices[fan_selector] == 'Level 75' and fan_current_speed ~= 0x4b and uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == 1 then
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		commandArray[fan_selector]='Set Level 30'
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> Fan speed isnt set properly, Fan speed corrected to 75%...</font>')	
		end
		end	

		if otherdevices[fan_selector] == 'Level 100' and fan_current_speed ~= 0x64 and uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == 1 then
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		commandArray[fan_selector]='Set Level 40'
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> Fan speed isnt set properly, Fan speed corrected to 100%...</font>')	
		end
		end		

--
-- **********************************************************
-- PIco Fan ON/OFF depending on RPI and UPS pcb temp
-- **********************************************************
--
		-- FAN OFF
		if (tonumber(otherdevices_svalues[rpi_temp]) <= 49.5) 
		and (uservariables[pico_fan_control_switch] == 0) and (otherdevices[fan_selector] ~= "Off")
		then
		commandArray[fan_selector]='Off'
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is okay, Fan state switched OFF...</font>')	
		end
		end

		-- FAN @ 25%
		if (tonumber(otherdevices_svalues[rpi_temp]) >= 50.0) and (tonumber(otherdevices_svalues[rpi_temp]) <= 55.0) 
		and (uservariables[pico_fan_control_switch] == 0) and (tonumber(otherdevices_svalues[fan_selector]) ~= 10)
		then
		commandArray[fan_selector]='Set Level 10'	
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is okay but Fan state is switched to 25%...</font>')	
		end	
		end

		-- FAN @ 50%
		if (tonumber(otherdevices_svalues[rpi_temp]) >= 55.5) and (tonumber(otherdevices_svalues[rpi_temp]) <= 60.0) 
		and (uservariables[pico_fan_control_switch] == 0) and (tonumber(otherdevices_svalues[fan_selector]) ~= 20)
		then
		commandArray[fan_selector]='Set Level 20'	
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is okay but starting to warm up, Fan state is switched to 50%...</font>')	
		end	
		end

		-- FAN @ 75%
		if (tonumber(otherdevices_svalues[rpi_temp]) >= 60.5) and (tonumber(otherdevices_svalues[rpi_temp]) <= 65.0) 
		and (uservariables[pico_fan_control_switch] == 0) and (tonumber(otherdevices_svalues[fan_selector]) ~= 30)
		then
		commandArray[fan_selector]='Set Level 30'	
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is getting to warm up, Fan state is switched to 75%...</font>')	
		end	
		end	

		-- FAN @ 100%
		if (tonumber(otherdevices_svalues[rpi_temp]) >= 65.5) and (tonumber(otherdevices_svalues[rpi_temp]) <= 75.0) 
		and (uservariables[pico_fan_control_switch] == 0) and (tonumber(otherdevices_svalues[fan_selector]) ~= 40)
		then
		commandArray[fan_selector]='Set Level 40'	
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is getting to HOT, Fan state is switched to 100%...</font>')	
		end	
		end	

		-- Temperature to HOT, System shut down
		if (tonumber(otherdevices_svalues[rpi_temp]) >= 75.5)
		then		
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> RPi temperature is to HOT, system shutdown...</font>')	
		commandArray['SendNotification']='Domoticz#System is to hot and has been shut down...'	
		commandArray[rpi_shutdown]='ON'
		commandArray['Variable:' .. pico_fan_control_switch] = '1'		
		end	
		end	

				
--
-- **********************************************************
-- PIco Fan time lock to prevent flip/flop
-- **********************************************************
--	 

		if uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes ~= tonumber(pico_fan_control_timeout) then
		pico_fan_control_minutes = pico_fan_control_minutes + 1
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> ' ..tonumber(pico_fan_control_remaining).. ' minutes remaining till '..fan_selector..' will be unlocked...</font>')	
		end
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		end	

		if uservariables[pico_fan_control_switch] == 1 and pico_fan_control_minutes == tonumber(pico_fan_control_timeout) then
		if verbose >= 1 then	
		print('<font color="darkred">-- '..fan_selector..' ==> '..fan_selector..' is now unlocked...</font>')	
		end
		pico_fan_control_minutes = 0
		commandArray['Variable:' .. pico_fan_control_counter] = tostring(pico_fan_control_minutes)
		commandArray['Variable:' .. pico_fan_control_switch] = '0'	
		end

end
