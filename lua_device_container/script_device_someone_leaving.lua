--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_someone_leaving.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-1-2017
	@Script to switch Garden Lights when ON/OFF when someone is leaving the house in the morning
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Devices															-- Various variables
	local phone_1 						= 'Jerina GSM'					local front_door_acivity			= 'IsFrontDoor_Acivity'
	local phone_2 						= 'Siewert GSM'
	local phone_3 						= 'Natalya GSM'				-- Scenes
	local phone_switch 					= 'Telefoons'					local garden_lights					= 'Tuinverlichting Vertrek'

-- Various Switches													-- Door/Window Sensors
	local someonehome					= 'Iemand Thuis'				local frontdoor						= 'Voor Deur'
	local someonehome_standby			= 'Iemand Thuis - Standby'		local backdoor			 			= 'Achter Deur'
	local nobody_home					= 'Niemand Thuis'				local sliding_door	 				= 'Schuifpui'
	local isdark_standby				= 'IsDonker - Standby'
	local leaving_standby				= 'Vertrek - Standby'		-- Light Switches
	local isdark_sunset					= 'Sunrise/Sunset'				local front_door_light				= 'Tuin Voordeur Verlichting'
	local isdark_living_room_trigger2	= 'IsDonker_Tuin_Verlichting'	local shed_lights					= 'Tuin Schuur Verlichting'

--
-- **********************************************************
-- Some one left via the frontdoor
-- **********************************************************
--

if devicechanged[frontdoor] == 'Open'
	and otherdevices[leaving_standby]   == 'Off'		
	and otherdevices[nobody_home]   == 'Off'	
	and otherdevices[front_door_light]   == 'Off' 	
	and otherdevices[shed_lights]   == 'Off'	
	and otherdevices[isdark_living_room_trigger2]   == 'On'
	and otherdevices[isdark_standby]   == 'On'	
	and otherdevices[isdark_sunset]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> Someone left via the '..frontdoor..', Scene:'..garden_lights..' switched ON...</font>')	
		end
		commandArray["Scene:" ..garden_lights.. ""]='On AFTER 2'	
		commandArray[leaving_standby]='On'		
end

--
--
-- **********************************************************
-- Some one left via backdoor
-- **********************************************************
--

if devicechanged[backdoor] == 'Open'
	and otherdevices[leaving_standby]   == 'Off'	
	and otherdevices[nobody_home]   == 'Off'	
	and otherdevices[front_door_light]   == 'Off' 
	and otherdevices[shed_lights]   == 'Off'	
	and otherdevices[isdark_living_room_trigger2]   == 'On'
	and otherdevices[isdark_standby]   == 'On'	
	and otherdevices[isdark_sunset]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> Someone left via the '..backdoor..', Scene:'..garden_lights..' switched ON...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='On AFTER 2'	
		commandArray[leaving_standby]='On'	
end

--
-- **********************************************************
-- Some one opened the sliding door
-- **********************************************************
--

if devicechanged[sliding_door] == 'Open'
	and otherdevices[nobody_home]   == 'Off'
	and otherdevices[leaving_standby]   == 'Off'		
	and otherdevices[front_door_light]   == 'Off' 
	and otherdevices[shed_lights]   == 'Off'	
	and otherdevices[isdark_living_room_trigger2]   == 'On'
	and otherdevices[isdark_standby]   == 'On'	
	and otherdevices[isdark_sunset]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> Someone opened the '..sliding_door..', Scene:'..garden_lights..' switched ON...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='On AFTER 2'	
		commandArray[leaving_standby]='On'		
end

if  devicechanged[sliding_door] == 'Closed' 
	and otherdevices[frontdoor] == 'Closed' 
	and otherdevices[backdoor] == 'Closed' 
	and otherdevices[shed_lights]   ~= 'Off' 
	and otherdevices[leaving_standby]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> Someone closed the '..sliding_door..', Scene:'..garden_lights..' switched OFF...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='Off AFTER 20'	
		commandArray[leaving_standby]='Off'		
end

--
-- **********************************************************
-- Some one leaving - Device offline
-- **********************************************************
--

if  devicechanged[phone_1] == 'Off'
	and otherdevices[shed_lights]   ~= 'Off'
	and otherdevices[leaving_standby]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> '..phone_1..' out of side, Scene:'..garden_lights..' switched OFF...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='Off AFTER 30'
		commandArray[leaving_standby]='Off AFTER 30'	
end

if  devicechanged[phone_2] == 'Off'
	and otherdevices[shed_lights]   ~= 'Off'
	and otherdevices[leaving_standby]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> '..phone_2..' out of side, Scene:'..garden_lights..' switched OFF...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='Off AFTER 30'
		commandArray[leaving_standby]='Off AFTER 30'			
end

if  devicechanged[phone_3] == 'Off'
	and otherdevices[shed_lights]   ~= 'Off'
	and otherdevices[leaving_standby]   == 'On'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> '..phone_3..' out of side, Scene:'..garden_lights..' switched OFF...</font>')	
		end		
		commandArray["Scene:" ..garden_lights.. ""]='Off AFTER 30'
		commandArray[leaving_standby]='Off AFTER 30'	
end

--
-- **********************************************************
-- Standby OFF When some one left
-- **********************************************************
--

if  devicechanged[leaving_standby] == 'Off' and otherdevices[shed_lights] ~= 'Off'
then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..leaving_standby..' ==> '..leaving_standby..' deactivated, Scene:'..garden_lights..' switched OFF...</font>')	
		end			
		commandArray["Scene:" ..garden_lights.. ""]='Off AFTER 30'		
end
