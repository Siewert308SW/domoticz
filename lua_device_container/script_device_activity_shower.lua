--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_activity_shower.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-13-2017
	@Script to switch OFF livingroom lights when one person at home and is taking a shower
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]]

-- Logging: 0 = None, 1 = All
local verbose = 1

-- Lights																-- IsDark Switches
	local shower_light					= 'Douche Lamp'						local isdark_dinner_table 			= 'IsDonker_Eettafel'
	local dinner_table_light 			= 'Woonkamer Eettafel Lamp'			local isdark_sunset					= 'Sunrise/Sunset'

-- Devices																-- Variables
	local phone_1 						= 'Jerina GSM'						local isshower_variable				= 'IsDouchen'
	local phone_2 						= 'Siewert GSM'						local iskerst						= 'Feestdagen'
	local phone_3 						= 'Natalya GSM'						local livingroom_lights_timeout		= 30
	local television 					= 'Televisie'						local dinnertable_lights_timeout	= 40	
	local laptop_1 						= 'Siewert Laptop'
	local laptop_2 						= 'Jerina Laptop'
	local laptop_3						= 'Natalya Laptop'
	
-- Door/Window Sensors													-- Various Switches
	local frontdoor						= 'Voor Deur'						local someonehome					= 'Iemand Thuis'
	local backdoor			 			= 'Achter Deur'						local network 						= 'Netwerk'
	local livingroom_door				= 'Kamer Deur'
	local sliding_door	 				= 'Schuifpui'					-- Scenes
	local scullery_door 				= 'Bijkeuken Deur'					local stage_2						= 'Woonkamer_Stage_2'
																			local stage_2_xmas					= 'Woonkamer_Kerst_2'

-- Determine how many phones are at home	
    if otherdevices[phone_1] == 'On' then p1=1 else p1=0 end
	if otherdevices[phone_2] == 'On' then p2=1 else p2=0 end
	if otherdevices[phone_3] == 'On' then p3=1 else p3=0 end
	   phones_home=p1 + p2 + p3

-- Determine how many laptops are online	
    if otherdevices[laptop_1] == 'On' then l1=1 else l1=0 end
	if otherdevices[laptop_2] == 'On' then l2=1 else l2=0 end
	if otherdevices[laptop_3] == 'On' then l3=1 else l3=0 end
	   laptops_online=l1 + l2 + l3	   
--
-- **********************************************************
-- Livingroom Lights ON/OFF when 1 person is at home and showering and IsNotXmas
-- **********************************************************
--

	if devicechanged[shower_light]   == 'On'
		and uservariables[isshower_variable] == 0
		and otherdevices[someonehome]   == 'On'	
		and otherdevices[television]   == 'Off'		
		and otherdevices[network]   == 'On'
		and otherdevices[frontdoor]   == 'Closed'
		and otherdevices[backdoor]   == 'Closed'
		and otherdevices[scullery_door]   == 'Closed'
		and otherdevices[livingroom_door]   == 'Closed'
		and otherdevices[iskerst] == 'Off'	
	then
		if phones_home == 1 and laptops_online == 1 then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> There seems to be just '..phones_home..' person at home and is probably taking a shower, Livingroom lights will be switched OFF in '..livingroom_lights_timeout..' seconds...</font>')	
			end	
			commandArray["Scene:" ..stage_2.. ""]='Off AFTER '..livingroom_lights_timeout..''
			commandArray[dinner_table_light]='Off AFTER '..dinnertable_lights_timeout..''
			commandArray["Variable:" .. isshower_variable .. ""]= '1'		
		else
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> There seems to be more then 1 person at home, while 1 is probably taking a shower, No changes to any lights events will be made...</font>')	
			end
	end
end

-- Someone entering the home via Frontdoor and reactivating the light scene
	if devicechanged[frontdoor]
		and uservariables[isshower_variable] == 1
		and otherdevices[iskerst] == 'Off'		
	then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> '..someonehome..' via '..frontdoor..', '..isshower_variable..' has been reset...</font>')	
			end	
			commandArray[someonehome]='On AFTER 5'
			commandArray["Variable:" .. isshower_variable .. ""]= '0'
	end
	
-- Someone entering the home via Backdoor and reactivating the light scene
	if devicechanged[backdoor]
		and uservariables[isshower_variable] == 1
		and otherdevices[iskerst] == 'Off'	
	then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> '..someonehome..' via '..backdoor..', '..isshower_variable..' has been reset...</font>')	
			end	
			commandArray[someonehome]='On AFTER 5'
			commandArray["Variable:" .. isshower_variable .. ""]= '0'
	end
	
-- Someone entering the home via Livingroom door and reactivating the light scene
	if devicechanged[livingroom_door]
		and uservariables[isshower_variable] == 1
		and otherdevices[iskerst] == 'Off'		
	then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> '..someonehome..' via '..livingroom_door..', '..isshower_variable..' has been reset...</font>')	
			end	
			commandArray[someonehome]='On AFTER 5'
			commandArray["Variable:" .. isshower_variable .. ""]= '0'
	end	

-- Someone entering the home via Sliding_door and reactivating the light scene	
	if devicechanged[sliding_door]
		and uservariables[isshower_variable] == 1
		and otherdevices[iskerst] == 'Off'	
	then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> '..someonehome..' via '..sliding_door..', '..isshower_variable..' has been reset...</font>')	
			end	
			commandArray[someonehome]='On AFTER 5'
			commandArray["Variable:" .. isshower_variable .. ""]= '0'
	end	

-- Someone entering the home via scullery_door and reactivating the light scene	
	if devicechanged[scullery_door]
		and uservariables[isshower_variable] == 1
		and otherdevices[iskerst] == 'Off'		
	then
			if verbose == 1 then	
			print('<font color="Darkblue">-- '..isshower_variable..' ==> '..someonehome..' via '..scullery_door..', '..isshower_variable..' has been reset...</font>')	
			end	
			commandArray[someonehome]='On AFTER 5'
			commandArray["Variable:" .. isshower_variable .. ""]= '0'
	end