--[[
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-

	script_device_lights_garden_shed.lua
	@author	: Siewert Lameijer
	@since	: 1-1-2015
	@updated: 1-15-2017
	@Script to switch garden light ON/OFF when IsDark taking in count IsWeekend or IsNotWeekend
	
-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
--]] 

-- Logging: 0 = None, 1 = All
local verbose = 1

-- IsDark Switches															-- Various Switches
	local isdark_garden_lights_trigger 	= 'IsDonker_Tuin_Verlichting'			local someonehome					= 'Iemand Thuis'
	local isdark_sunset					= 'Sunrise/Sunset'						local someonehome_standby			= 'Iemand Thuis - Standby'
	local isdark_standby				= 'IsDonker - Standby'					local nobody_home					= 'Niemand Thuis'
																				local leaving_standby				= 'Vertrek - Standby'

-- Variables																-- Light Switches
	local isdark_garden_lights_variable	= 'IsDonker_Tuin_Verlichting_Standby'	local back_garden_lights			= 'Tuin Schuur Verlichting'
	local isweekend_variable			= 'Var_IsWeekend'						local front_door_light				= 'Tuin Voordeur Verlichting'
	local garden_lights_verify			= 'Tuin_Verlichting_Verify'				local garden_light_switch			= 'Tuin Verlichting Knop'
	local garden_scene_lights_timeout	= 30
	local pico_power_reset				= 'IsPIco_Power_Reset'
	
-- Various Switches															-- Scenes
	local someonehome					= 'Iemand Thuis'						local garden_lights_scene			= 'Tuinverlichting'
	local someonehome_standby			= 'Iemand Thuis - Standby'				local garden_lights_backup_scene	= 'Tuin Verlichting - Backup'

--
-- **********************************************************
-- Garden light ON when dark
-- **********************************************************
--

	if devicechanged[isdark_garden_lights_trigger] == 'On'
		and otherdevices[isdark_sunset] == 'On'
		and otherdevices[back_garden_lights] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> '..isdark_garden_lights_trigger..' activated, '..back_garden_lights..' & '..front_door_light..' switched ON...</font>')	
		end		
		commandArray[back_garden_lights]='On AFTER 10'
		commandArray[front_door_light]='Set Level 7 AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='On AFTER 30'		
	end

--
-- **********************************************************
-- Garden light ON when dark standby ON
-- **********************************************************
--

	if devicechanged[isdark_sunset] == 'On'
		and otherdevices[isdark_garden_lights_trigger] == 'On'
		and otherdevices[back_garden_lights] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> '..isdark_sunset..' activated, '..back_garden_lights..' & '..front_door_light..' switched ON...</font>')	
		end			
		commandArray[back_garden_lights]='On AFTER 40'
		commandArray[front_door_light]='Set Level 7 AFTER 50'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='On AFTER 60'	
	end

--
-- **********************************************************
-- Garden light standby ON when some one home
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 15)
		and uservariables[isdark_garden_lights_variable] == 0	
		and otherdevices[someonehome] =='On'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> 22:15hr standby schedule: Set '..isdark_garden_lights_variable..'...</font>')	
		end		
		commandArray["Variable:" .. isdark_garden_lights_variable .. ""]= '1'
	end
	
--
-- **********************************************************
-- Garden light OFF when some one at home
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 50)
		and otherdevices[isdark_garden_lights_trigger] == 'On'
		and otherdevices[back_garden_lights] ~= 'Off'
		and uservariables[isdark_garden_lights_variable] == 1
		and uservariables[isweekend_variable] == 0		
		and otherdevices[someonehome] =='On'
		and uservariables[garden_lights_verify]   == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> 22:50hr weekday schedule: switched OFF '..back_garden_lights..' & '..front_door_light..'...</font>')	
		end		
		commandArray[back_garden_lights]='Off AFTER 10'
		commandArray[front_door_light]='Off AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='Off AFTER 25'	
		commandArray[garden_light_switch]='Off AFTER 30'
		commandArray["Variable:" .. garden_lights_verify .. ""]= '1'		
	end	
	
--
-- **********************************************************
-- Garden light OFF when nobody is home and IsNotWeekend
-- **********************************************************
--

	if (time.hour == 22) and (time.min == 50)
		and otherdevices[isdark_garden_lights_trigger] == 'On'
		and otherdevices[back_garden_lights] ~= 'Off'
		and uservariables[isdark_garden_lights_variable] == 0
		and otherdevices[someonehome] =='Off'
		and uservariables[garden_lights_verify]   == 0
		and uservariables[isweekend_variable] == 0		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> 22:50hr weekday schedule: switched OFF '..back_garden_lights..' & '..front_door_light..'...</font>')	
		end				
		commandArray[back_garden_lights]='Off AFTER 10'
		commandArray[front_door_light]='Off AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='Off AFTER 25'	
		commandArray[garden_light_switch]='Off AFTER 30'
		commandArray["Variable:" .. garden_lights_verify .. ""]= '1'		
	end	

--
-- **********************************************************
-- Garden light OFF when nobody is home and IsWeekend
-- **********************************************************
--

	if (time.hour == 23) and (time.min == 30)
		and otherdevices[isdark_garden_lights_trigger] == 'On'
		and otherdevices[back_garden_lights] ~= 'Off'
		and uservariables[isdark_garden_lights_variable] == 0
		and otherdevices[someonehome] =='Off'
		and uservariables[garden_lights_verify]   == 0
		and uservariables[isweekend_variable] == 1		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> 23:30hr weekend schedule: switched OFF '..back_garden_lights..' & '..front_door_light..'...</font>')	
		end				
		commandArray[back_garden_lights]='Off AFTER 10'
		commandArray[front_door_light]='Off AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='Off AFTER 25'	
		commandArray[garden_light_switch]='Off AFTER 30'
		commandArray["Variable:" .. garden_lights_verify .. ""]= '1'		
	end

--
-- **********************************************************
-- Garden light OFF when IsDark OFF
-- **********************************************************
--

	if devicechanged[isdark_garden_lights_trigger] == 'Off'
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> '..isdark_garden_lights_trigger..' deactivated, '..back_garden_lights..' & '..front_door_light..' switched OFF...</font>')	
		end		
		commandArray[back_garden_lights]='Off AFTER 10'
		commandArray[front_door_light]='Off AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='Off AFTER 25'	
		commandArray[garden_light_switch]='Off AFTER 30'
		commandArray["Variable:" .. garden_lights_verify .. ""]= '0'
		commandArray["Variable:" .. isdark_garden_lights_variable .. ""]= '0'
	end

--
-- **********************************************************
-- Garden lights OFF when Nobody at home
-- **********************************************************
--

	if devicechanged[someonehome_standby] == 'Off'	
		and otherdevices[someonehome]  == 'Off'	
		and otherdevices[leaving_standby]   == 'Off'		
		and otherdevices[back_garden_lights] ~= 'Off'
		and uservariables[isdark_garden_lights_variable] == 1		
	then
		if verbose == 1 then	
		print('<font color="Darkblue">-- '..back_garden_lights..' ==> '..someonehome_standby..' deactivated, Scene:'..garden_lights_scene..' will be switched OFF in '..garden_scene_lights_timeout..' seconds...</font>')	
		end		
		commandArray[back_garden_lights]='Off AFTER 10'
		commandArray[front_door_light]='Off AFTER 20'	
		commandArray["Scene:" ..garden_lights_backup_scene.. ""]='Off AFTER 25'	
		commandArray[garden_light_switch]='Off AFTER 30'
	end

--
-- **********************************************************
-- Restore garden lights state when power outage is over
-- **********************************************************
--	

	if devicechanged[someonehome] == 'On'
		and otherdevices[isdark_sunset] == 'On'	
		and otherdevices[isdark_garden_lights_trigger] == 'On'	
		and uservariables[isdark_garden_lights_variable] == 0		
		and uservariables[pico_power_reset] == 1		
	then
		if verbose == 1 then	
		print('<font color="Blue">-- '..back_garden_lights..' ==> '..pico_power..' activated, '..back_garden_lights..' switched ON...</font>')	
		end
		commandArray[front_border_lights]='Set Level 50 AFTER 50'
	end